var id;
var figureNumber;
var model;

$(document).ready(function() {
//    var Request = new Object();
//    Request = GetRequest();
//    id = Request['id'];
	id = parent.$("#transmit_id").val();
	figureNumber = parent.$("#transmit_figureNumber").val();
	model = parent.$("#transmit_model").val();
    
	//页面赋值
    $.ajax({
        url: "../login/getUserSession.do",
        type: "POST",
        dataType: "json",
        data:{},
        async:false,
        success: function(result) {
            if(result.ret == '1') {
                var a = result.data.username;
                var b = result.data.password;
                type = result.data.userType;
                $("#applicant").val(result.data.username);
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });
    
	//页面赋值
    $("#figureNumber").val(figureNumber);
    $("#model").val(model);
    $("#reportRepairDate").val(getNowDate());
    $("#reportRepairTime").val(getNowTime());
    
});


//点击取消按钮
function layer_close(){
    window.location.href = "equipment-list.html";
}

//点击提交
function save() {
	var flag = "";
	//验证
	var formValidation = fv();
	if(!formValidation.flag){
		return formValidation.msg;
	}

	//取值
	var data = getValue();

      $.ajax({
  		url: '../mould/repairMould.do',
  		type: 'POST',
  		data: data,
        dataType : "json",
        async:false,
  		beforeSend: function (XMLHttpRequest) {

        },
        complete: function (XMLHttpRequest, textStatus) {

        },
  		success: function(result) {
  			if(result.ret == '1') {
  				flag = 1;
  			} else {
  				  
  				var error = "";
  				for(var i = 0; i < result.data.length; i++) {
  					error += (result.data[i].message + "<br\>");
  				}
  				if(error != "") {
  					layer.msg(error);
  					flag = error;
  				}				
  			}   
  		},
  		error: function(XMLHttpRequest, textStatus, errorThrown) {
  			alert("error");
  		}

  	});
      
      return flag;
}

function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}

//表单验证
function fv(){
	var flag = {};
	flag["flag"] = true;
	
	var boolCheck = $('input:radio[name="failurePeriod"]').is(":checked"); 
	if(!boolCheck){
		flag["msg"] = "请选择故障时期";
		flag["flag"] = false;
	}
	
	var phenomenalDescription = $("#phenomenalDescription").val();
	phenomenalDescription = $.trim(phenomenalDescription);
	if(phenomenalDescription == ""){
		flag["msg"] = "请填写故障内容";
		flag["flag"] = false;
	}
	
	var applicant = $("#applicant").val();
	if(applicant == ""){
		flag["msg"] = "登录过期,请重新登录";
		flag["flag"] = false;
	}
	
	return flag;
}

//页面取值
function getValue(){
	var data = {};
    data["mouldId"] = id;
    data["applicant"] = $("#applicant").val();
//    data["reportRepairTime"] = $("#reportRepairDate").val() + "  " + $("#reportRepairTime").val();
    data["failurePeriod"] = $("input:radio[name='failurePeriod']:checked").val();
    data["phenomenalDescription"] = $.trim($("#phenomenalDescription").val());
    return data;
}
