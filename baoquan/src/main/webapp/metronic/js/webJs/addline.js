//保存
function save() {
    var beltlineName = $("#beltlineName").val(),
        beltlineDescription = $("#beltlineDescription").val();
    if (isNullorEmpty(beltlineName)) {
        layer.alert("生产线名称不能为空");
        return;
    }
    $.ajax({
        url: "../line/add.do",
        type: "POST",
        dataType: "json",
        data: {
            "beltlineName": beltlineName,
            "beltlineDescription": beltlineDescription
        },
        async:false,
        beforeSend: function (XMLHttpRequest) {
            $("#save").attr('disabled',false);
        },
        complete: function (XMLHttpRequest, textStatus) {

        },
        success: function (result) {
            if (result.ret == '1') {
                window.location.href="produce-list.html";
                // layer.msg("保存成功");
                // setTimeout(function(){
                //     window.location.href="produce-list.html";
                // }, 2000 );
            } else {
                var error = "";
                for (var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
                $("#save").removeAttr('disabled');
            }
        }
    });
}
//取消
function layer_close(){
    window.location.href = "produce-list.html";
}