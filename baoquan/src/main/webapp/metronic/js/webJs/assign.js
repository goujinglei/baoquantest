$(document).ready(function() {
    var Request = new Object();
    Request = GetRequest();
    var id = Request['id'];
    query(id);

});

function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}

function query(id) {
    $("#reId").val(id);
    //获取人员列表
    $.ajax({
        url: "../user/getBaoquan.do",
        type: "POST",
        dataType: "json",
        data: {
        },
        success: function (result) {
            if (result.ret == '1') {
                var list = result.data;
                for (var i = 0; i < list.length; i++) {
                    // 为Select下选框赋值
                    //先创建好select里面的option元素
                    var option = document.createElement("option");
                    //转换DOM对象为JQ对象,好用JQ里面提供的方法 给option的value赋值
                    $(option).val(list[i].username);
                    //给option的text赋值,这就是你点开下拉框能够看到的东西
                    $(option).text(list[i].username);
                    //获取select 下拉框对象,并将option添加进select
                    $('#workMan').append(option);
                }
                //去重
                $('#workMan').each(function(i,n){
                    var options = "";
                    $(n).find("option").each(function(j,m){
                        if(options.indexOf($(m)[0].outerHTML) == -1)
                        {
                            options += $(m)[0].outerHTML;
                        }
                    });
                    $(n).html(options);
                });
            } else {
                var error = "";
                for (var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                alert(error);
            }
        }
    });
}

function save() {
    var workMan = $("#workMan option:selected").text();
    var id = $("#reId").val();
    //更新repair
    $.ajax({
        url: "../repair/update.do",
        type: "POST",
        dataType: "json",
        async:false,
        data:{
            "id":id,
            "workMan":workMan,
            "state":9
        },
        success: function(result) {
            if(result.ret == '1') {
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });
    // 更新equip_repair
    var eqReData={
        "id":Number(id),
        "maintainer":workMan,
        "mainTaskMan":workMan
    };
    var jsonData = JSON.stringify(eqReData);

    $.ajax({
        url: "../equipRepair/update.do",
        type: "POST",
        dataType: "json",
        data:jsonData,
        contentType: "application/json; charset=utf-8",
        success: function(result) {
            if(result.ret == '1') {
                layer.msg("指派成功");
                setTimeout(function(){
                    parent.window.location.href="serviceman.html";
                }, 2000 );
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });
}