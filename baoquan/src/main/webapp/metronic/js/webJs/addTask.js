$(document).ready(function() {

});

//设备保存
$("#saveEquipTask").click(function() {
	var spotName = $("#spotName").val(),
		beginTime = $("#beginTime").val();
	var spotType = $("#spotType option:selected").val();
	var year = $("#year option:selected").val();

	if(isNullorEmpty(spotName)) {
		layer.alert("任务名称不能为空");
		return;
	}
	if(isNullorEmpty(beginTime)) {
		layer.alert("任务开始时间不能为空");
		return;
	}
	if(spotType == "9") {
		layer.alert("请选择任务类型");
		return;
	}

	//创建次数
	var times = 0;
	//任务周期
	var spotInterval;
	//设备或模具id集合  
	var ids = new Array();

	if(spotType == "1") {

		spotInterval = $("#equiqInterval").val();
		if(spotInterval == 9) {
			layer.alert("任务周期不能为空");
			return;
		}

		$("#menuFuncBase option").each(function() {
			//遍历所有option  
			var id = $(this).val(); //获取option值   
			ids.push(id);
		});

		if(ids == null || ids.length == 0) {
			layer.alert("请选择设备");
			return;
		}

		times = Number(year) * 12 / Number(spotInterval);
	} else if(spotType == "2") {

		spotInterval = $("#mouldInterval").val();
		if(isNullorEmpty(spotInterval)) {
			layer.alert("任务周期不能为空");
			return;
		}
	}

	$.ajax({
		url: "../task/add.do",
		type: "POST",
		dataType: "json",
		data: {
			"spotName": spotName,
			"spotType": spotType,
			"spotInterval": spotInterval,
			"beginTime": beginTime,
			"state": 0,
			"times": times,
			"ids": ids
		},
		beforeSend: function(XMLHttpRequest) {
			$("#saveEquipTask").attr('disabled', false);
			$("#saveMouldTask").attr('disabled', false);
		},
		complete: function(XMLHttpRequest, textStatus) {
//			$("#saveEquipTask").removeAttr('disabled');
//			$("#saveMouldTask").removeAttr('disabled');
		},
		success: function(result) {
			if(result.ret == '1') {
				if(result.data == null){
//					layer.msg("保存成功");
//					setTimeout(function() {
						window.location.href = "spotTask.html"
//					}, 2000);
				}else{
					$("#saveEquipTask").removeAttr('disabled');
					$("#saveMouldTask").removeAttr('disabled');
					layer.alert(result.data);
				}

			} else {
				$("#saveEquipTask").removeAttr('disabled');
				$("#saveMouldTask").removeAttr('disabled');
				var error = "";
				for(var i = 0; i < result.data.length; i++) {
					error += (result.data[i].message);
				}
				layer.alert(error);
			}
		}
	});
})

//模具保存
$("#saveMouldTask").click(function() {
	var spotName = $("#spotName").val(),
		beginTime = $("#beginTime").val();
	var spotType = $("#spotType option:selected").val();
	var year = $("#year option:selected").val();

	if(isNullorEmpty(spotName)) {
		layer.alert("任务名称不能为空");
		return;
	}
	if(isNullorEmpty(beginTime)) {
		layer.alert("任务开始时间不能为空");
		return;
	}
	if(spotType == "9") {
		layer.alert("请选择任务类型");
		return;
	}

	//
	if(!$("input[type='checkbox']").is(":checked")) {
		layer.msg('请选择模具');
		return;
	}
	
	//设备或模具id集合  
	var ids = new Array();
	var annualOutput = 0;
    $('input[name="mouldCheckBox"]:checked').each(function(){//遍历每一个名字为interest的复选框，其中选中的执行函数    
    	ids.push($(this).val());//将选中的值添加到数组chk_value中    
    	var tatr = $(this).parent().parent();
    	var tatd = tatr.children("td").eq(2);
    	annualOutput = tatd.text();
    });
	
	//创建次数
	var times = 0;
	//任务周期
	var spotInterval = $("#mouldInterval").val();

	if(isNullorEmpty(spotInterval)) {
		layer.alert("任务周期不能为空");
		return;
	}
	
	times = Math.floor(annualOutput * year/spotInterval);
		
	$.ajax({
		url: "../task/add.do",
		type: "POST",
		dataType: "json",
		data: {
			"spotName": spotName,
			"spotType": spotType,
			"spotInterval": spotInterval,
			"beginTime": beginTime,
			"state": 0,
			"times": times,
			"ids": ids
		},
		beforeSend: function(XMLHttpRequest) {
			$("#saveEquipTask").attr('disabled', false);
			$("#saveMouldTask").attr('disabled', false);
		},
		complete: function(XMLHttpRequest, textStatus) {
//			$("#saveEquipTask").removeAttr('disabled');
//			$("#saveMouldTask").removeAttr('disabled');
		},
		success: function(result) {
			if(result.ret == '1') {
				if(result.data == null){
//					layer.msg("保存成功");
//					setTimeout(function() {
						window.location.href = "spotTask.html"
//					}, 2000);
				}else{
					$("#saveEquipTask").removeAttr('disabled');
					$("#saveMouldTask").removeAttr('disabled');
					layer.alert(result.data);

				}
			} else {
				$("#saveEquipTask").removeAttr('disabled');
				$("#saveMouldTask").removeAttr('disabled');
				var error = "";
				for(var i = 0; i < result.data.length; i++) {
					error += (result.data[i].message);
				}
				layer.alert(error);
			}
		}
	});

})

//点击取消按钮
function layer_close() {
	window.location.href = "spotTask.html";
}

$("#spotType").change(function() {
	if($(this).val() == 1) {

		$("#equipCycle").show();
		$("#mouldCycle").hide();
		$("#equipChoose").show();
		$("#equipHtmlToSearch").show();
		$("#saveEquipTask").show();
		$("#saveMouldTask").hide();
		$("#mouldHtmlToSearch").hide();
		$("#mouldTable").hide();

	} else if($(this).val() == 2) {

		$("#equipCycle").hide();
		$("#mouldCycle").show();
		$("#equipChoose").hide();
		$("#equipHtmlToSearch").hide();
		$("#saveEquipTask").hide();
		$("#mouldHtmlToSearch").show();
		//		$("#mouldTable").show();
	} else {

		$("#equipCycle").hide();
		$("#mouldCycle").hide();
		$("#equipChoose").hide();
		$("#equipHtmlToSearch").hide();
		$("#saveEquipTask").hide();
		$("#saveMouldTask").hide();
		$("#mouldHtmlToSearch").hide();
		$("#mouldTable").hide();
	}

})

//设备搜索
function equipModelSearch() {
	var equipModel = $("#equipModel").val();

	var ids = new Array();
	$("#menuFuncBase option").each(function() {
		//遍历所有option  
		var id = $(this).val(); //获取option值   
		ids.push(id);
	});

	$.ajax({
		url: '../spotRule/selectEquipModelListByEquipModel.do',
		type: 'POST',
		data: {
			equipModel: equipModel,
			ids: ids
		},
		beforeSend: function(XMLHttpRequest) {

		},
		complete: function(XMLHttpRequest, textStatus) {

		},
		success: function(result) {
			if(result.ret == '1') {

				var selectList = result.data;
				selectVaules(selectList);

			} else {
				var error = "";
				for(var i = 0; i < result.data.length; i++) {
					error += (result.data[i].message + "<br\>");
				}
				if(error != "") {
					layer.msg(error);
				}
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert("error");
		}

	});
}

//模具搜索
function mouldModelSearch() {
	var vehicleType = $("#vehicleType").val();
	var figureNumber = $("#figureNumber").val();

	var flag = true;
	if(vehicleType == "" || vehicleType == null) {
		layer.msg("请输入车种");
		flag = false;
	}
	if(figureNumber == "" || figureNumber == null) {
		layer.msg("请输入图号");
		flag = false;
	}

	if(!flag) {
		return;
	}

	$.ajax({
		url: '../spotRule/selectMouldByVehicleTypeAndFigureNumber.do',
		type: 'POST',
		data: {
			vehicleType: vehicleType,
			figureNumber: figureNumber
		},
		beforeSend: function(XMLHttpRequest) {

		},
		complete: function(XMLHttpRequest, textStatus) {

		},
		success: function(result) {
			if(result.ret == '1') {

				var data = result.data;
				if(data != null) {
					var div = "<tr class='text-c'>" +
						"<td><input type='checkbox' name='mouldCheckBox' value='" + data.id + "'/></td>" +
						"<td>" + data.factoryName + "</td>" +
						"<td>" + data.annualOutput + "</td>" +
						"<td>" + data.vehicleType + "</td>" +
						"<td>" + data.figureNumber + "</td>" +
						"<td>" + data.assetCoding + "</td>" +
						"<td>" + data.model + "</td>" +
						"</td>" +
						"</tr>";
					$("#mouleList").empty();
					$("#mouleList").append(div);
					$("#mouldTable").show();
					$("#saveMouldTask").show();

				} else {
					$("#mouleList").empty();
					layer.msg("无此模具！");
				}

			} else {
				var error = "";
				for(var i = 0; i < result.data.length; i++) {
					error += (result.data[i].message + "<br\>");
				}
				if(error != "") {
					layer.msg(error);
				}
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert("error");
		}

	});
}

function selectVaules(selectList) {

	$("#menuFunc").empty();

	if(selectList != null && selectList.length > 0) {
		var menuFunc = "";
		$(selectList).each(function(i, e) {

			menuFunc += "<option  value='" + e.id + "'>" + e.name + "</option>";

		});
		$("#menuFunc").append(menuFunc);
	}

}