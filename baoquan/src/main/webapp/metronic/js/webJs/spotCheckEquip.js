$(document).ready(function() {
    var Request = new Object();
    Request = GetRequest();
    var id = Request['id'];
    var model = Request['model'];
    var cycle = Request['cycle'];
    var equipId = Request['equipId'];
    $("#id").val(id);
    $("#model").val(model);
    $("#cycle").val(cycle);
    $.ajax({
        url: "../login/getUserSession.do",
        type: "POST",
        dataType: "json",
        data:{},
        async:false,
        success: function(result) {
            if(result.ret == '1') {
                $("#username").val(result.data.username);
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });
    query(id,model,equipId);
});
function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}
function query(id,model,equipId) {

    $.ajax({
        url: "../equip/getEquipByid.do",
        type: "POST",
        dataType: "json",
        data: {
            "id": Number(equipId)
        },
        async:false,
        success: function (result) {
            if (result.ret == '1') {
                $('#equipName').val(result.data.equipName);
                $('#equipModel').val(result.data.equipModel);
                $('#standard').val(result.data.standard);
                $("#equipNum").val(result.data.equipNum);
            } else {
                var error = "";
                for (var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                alert(error);
            }
        }
    });

    var cycle = $("#cycle").val();
    $.ajax({
        url: "../spotCheck/getCheck.do",
        type: "POST",
        dataType: "json",
        data:{
            "spotDetailId":id,
            "principalNumbe":model,
            "spotInterval":cycle,
            "type":1
        },
        async:false,
        success: function(result) {
            if(result.ret == '1') {
                $("#checkList").html("");
                var checkList = result.data;
                for(var i = 0; i < checkList.length; i++) {
                    var str = "spotState"+i;
                    var data = checkList[i];
                    var j = i+1;
                    var div  = "<tr class='text-c'>" +
                        "<td>" + j + "</td>" +
                        "<td>"+ data.spotPosition +"</td>" +
                        "<td>"+data.checkMethod +"</td>" +
                        "<td>" + data.checkProject + "</td>" +
                        "<td>"+toInterval(data.spotInterval,i)+"</td>" +
                        "<td contentEditable='true'>" + checkplanTime(data.planTime) + "</td>" +
                        "<td style='width: 10%'><select name='spotState' id='"+str+"' class='select'><option  value='9'>请选择</option>"+
                        "<option  value='1'>计划</option>"+
                        "<option  value='2'>完成</option>"+
                        "<option  value='3'>不良要修理(不急)</option>"+
                        "<option  value='4'>不良要修理(至急)</option>"+
                        "</select>" +"</td>" +
                        "<td>" + checkspotTime(data.spotTime,i) +"</td>" +
                        "<td style='display: none'>"+ data.id +"</td>" +
                        "</tr>";
                    $("#checkList").append(div);
                    var all_options = document.getElementById(str).options;
                    for (var p=0; p<all_options.length; p++){
                        if (Number(all_options[p].value) == data.spotState) {
                            all_options[p].selected = true;
                        }
                    }
                }
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
            }
        }
    });
}

function checkplanTime(planTime) {
    if(planTime==null){
        return "";
    }else{
        return planTime;
    }
}

function checkspotTime(spotTime,j) {
    var str = "spotTime"+j;
    if(spotTime==null){
        return "<input type='date' id='"+str+"' name='spotTime' class='input-text'>";
    }else{
        return "<input type='date' id='"+str+"' name='spotTime' class='input-text' value='"+spotTime +"'>";
    }
}

function toInterval(interval) {
    if(interval=='1'){
        return "月";
    }else if(interval=='3'){
        return "季度";
    }else if(interval=='6'){
        return "半年";
    }else if(interval=='12'){
        return "年";
    }
}

function initComBox(selectList, htmlId){

    var optionHtml = "<option value='9'>请选择</option>";
    $(selectList).each(function(i, e) {

        optionHtml += '<option value="' + e.id + '">' + e.name + '</option>';
    });
    $("#" + htmlId).append(optionHtml);

}

function save() {
    var jdList = [];
    var flag = true;
    $("#checkList tr").each(function (i) {
        var jd = {};
        $(this).children('td').each(function (j) {
            if (j == 5) {
                var planTime = $(this).text();
                jd["planTime"] = Number(planTime);
                if(planTime==null||planTime==""||planTime=="0"){
                    flag = false;
                }
            }
            else if (j == 6) {
                var str = "#spotState"+i+" option:selected";
                var spotState = $(str).val();
                jd["spotState"] = Number(spotState);
                if(spotState=="9"){
                    flag = false;
                }
            }
            else if (j == 7) {
                var str = "#spotTime"+i;
                var spotTime = $(str).val();
                jd["spotTime"] = spotTime;
                if(spotTime==null||spotTime==""){
                    flag = false;
                }
            }
            else if (j == 8) {
                var id = $(this).text();
                jd["id"] = Number(id);
            }
        });
            jdList.push(jd);
    });

    var jdListJson = JSON.stringify(jdList);

    $.ajax({
        url: '../spotCheck/update.do',
        type: 'POST',
        data: jdListJson,
        async:false,
        contentType: "application/json; charset=utf-8",
        beforeSend: function (XMLHttpRequest) {

        },
        complete: function (XMLHttpRequest, textStatus) {

        },
        success: function(result) {
            if(result.ret == '1') {
                layer.msg("保存成功");
                setTimeout(function(){
                    window.location.href="spotTask.html"
                }, 2000 );
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message + "<br\>");
                }
                if(error != "") {
                    layer.msg(error);
                }
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("error");
        }
    });

    //更新状态
    if(flag){
        $.ajax({
            url: "../spotDetail/updateState.do",
            type: "POST",
            dataType: "json",
            async:false,
            data:{
                "id":Number($("#id").val())
            },
            success: function(result) {
                if(result.ret == '1') {
                } else {
                    var error = "";
                    for(var i = 0; i < result.data.length; i++) {
                        error += (result.data[i].message);
                    }
                    layer.alert(error);
                }
            }
        });
    }
}