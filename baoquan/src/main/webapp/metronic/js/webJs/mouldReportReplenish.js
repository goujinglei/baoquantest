var id;

$(document).ready(function() {
    var Request = new Object();
    Request = GetRequest();
    id = Request['id'];

	//页面赋值
    $.ajax({
        url: "../mould/selectRepairMouldByRepairId.do",
        type: "POST",
        dataType: "json",
        data:{"id": id},
        async:false,
        success: function(result) {
            if(result.ret == '1') {
               var data = result.data;
               var reportdate = data.reportRepairTime.split(" ");
               $("#reportRepairDate").val(reportdate[0]);
               $("#reportRepairTime").val(reportdate[1]);
               $("#applicant").val(data.applicant);
               $("#figureNumber").val(data.figureNumber);
               $("#model").val(data.model);
               $("#phenomenalDescription").val(data.phenomenalDescription);
			   $("input:radio[value='"+ data.failurePeriod +"']").attr('checked','true');
			   $("#id").val(data.id);
			   $("#reId").val(data.reId);
			   $("#reason").val(data.reason);
			   $("#emergencyDisposal").val(data.emergencyDisposal);			   
			   $("#permanentGame").val(data.permanentGame);
			   
			   var beginTime = data.beginTime.split(" ");
			   $("#beginTime").val(beginTime[0]);
			   $("#beginTimeToTime").val(beginTime[1]);
			   
			   var endTime = data.endTime.split(" ");
			   $("#endTime").val(endTime[0]);
			   $("#endTimeToTime").val(endTime[1]);
			   
			   var orderDate = data.orderTime.split(" ");
               $("#orderTime").val(orderDate[0]);
               $("#orderTimeToTime").val(orderDate[1]);
			   
			   //给维修用时累计赋值 
               if(data.faultLocationUrl != null && data.faultLocationUrl !=""){
            	   maintenanceTimeView(data.beginTime, data.endTime);
        		   
               }
			  
               //追加图片显示
			   pictureView(data.faultLocationUrl);
			   
               
               
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });
    $("#uploadInf").hide();
});


//点击取消按钮
function layer_close(){
    window.location.href = "equipment-list.html";
}

//点击提交
function save() {

	//验证
	var formValidation = fv();
	if(!formValidation){
		return;
	}

	//取值
	var data = getValue();

      $.ajax({
  		url: '../mould/updateRepairMould.do',
  		type: 'POST',
  		data: data,
        dataType : "json",
        async:false,
  		beforeSend: function (XMLHttpRequest) {
            $("#saveForm").attr('disabled',false);
        },
        complete: function (XMLHttpRequest, textStatus) {
        	$("#saveForm").removeAttr('disabled');
        },
  		success: function(result) {
  			if(result.ret == '1') {
//                layer.msg("保存成功");
//				setTimeout(function(){
					window.location.href= "serviceman.html"
//				}, 2000 );
  			} else {
  	        	$("#saveForm").removeAttr('disabled');
  				var error = "";
  				for(var i = 0; i < result.data.length; i++) {
  					error += (result.data[i].message + "<br\>");
  				}
  				if(error != "") {
  					layer.msg(error);
  					flag = error;
  				}				
  			}   
  		},
  		error: function(XMLHttpRequest, textStatus, errorThrown) {
        	$("#saveForm").removeAttr('disabled');
  			alert("error");
  		}

  	});

}

function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}


function custom_close(){
	window.location.href= "serviceman.html";
}

function maintenanceTimeView(beginTime, endTime){
	var start = new Date(beginTime);
	var end = new Date(endTime);
	
	var num = (end-start)/(1000*60);

	if(!isNaN(num)){
		$("#maintenanceTime").val(num + "分钟");
	}else{
		$("#maintenanceTime").val("");
	}
}


//表单验证
function fv(){
	var flag = true;
	
	var boolCheck = $('input:radio[name="failurePeriod"]').is(":checked"); 
	if(!boolCheck){
		layer.alert("请选择故障时期");
		flag = false;
	}
	
	var phenomenalDescription = $("#phenomenalDescription").val();
	phenomenalDescription = $.trim(phenomenalDescription);
	if(phenomenalDescription == ""){
		layer.alert("请填写故障内容");
		flag = false;
	}
	
	var beginTime = $("#beginTime").val();
	if(beginTime == ""){
		layer.alert("请填写维修开始日期");
		flag = false;
	}
	
	var beginTimeToTime = $("#beginTimeToTime").val();
	if(beginTimeToTime == ""){
		layer.alert("请填写维修开始时间");
		flag = false;
	}
	
	var endTime = $("#endTime").val();
	if(endTime == ""){
		layer.alert("请填写维修结束日期");
		flag = false;
	}
	
	var endTimeToTime = $("#endTimeToTime").val();
	if(endTimeToTime == ""){
		layer.alert("请填写维修结束时间");
		flag = false;
	}
	
	var applicant = $("#applicant").val();
	if(applicant == ""){
		layer.alert("登录过期,请重新登录");
		flag = false;
	}
	
	return flag;
}


function pictureView(url){
	var urls = url.split(",");
	if(urls != "" && urls.length > 0){
		for(var i=0; i < urls.length; i++){
			var picturUrl = urls[i];
			var html = "<p>" +
					   	  "<a href='#' onclick=\"view('"+ picturUrl +"')\" style='margin-left:50px;width:100px;' >图片"+ (i+1) +"&nbsp;&nbsp;&nbsp;&nbsp;点击查看</a>" +
					   	  "<input type='text' name='fileUrls' value='"+ picturUrl +"' style='display:none'/>" +
						  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type='button' onclick='delPictur(this)' class='btn btn-primary radius'>删除当前图片</button>" +
					   "</p>";	  
			$("#pictureView").append(html);
		}
	}

}

function view(url){

	window.open(url);
}

//删除当前照片
function delPictur(obj){
	$(obj).parent().remove();
}

//页面取值
function getValue(){
	var data = {};
    data["id"] = $("#id").val();
    data["reId"] = $("#reId").val();
    data["applicant"] = $("#applicant").val();
    data["reportRepairTime"] = $("#reportRepairDate").val() + "  " + $("#reportRepairTime").val();
    data["failurePeriod"] = $("input:radio[name='failurePeriod']:checked").val();
    data["phenomenalDescription"] = $.trim($("#phenomenalDescription").val());
    data["reason"] = $.trim($("#reason").val());
    data["emergencyDisposal"] = $.trim($("#emergencyDisposal").val());
    data["permanentGame"] = $.trim($("#permanentGame").val());
    
    var fileUrls = "";
    var obj = $("input[name='fileUrls']");
    if(obj.length > 0){
        $(obj).each(function(j,item){
            console.log("下标:"+j);
            console.log("value值:"+item.value);
            fileUrls += j > 0 ? ","+item.value : item.value;
          });
    }
    data["faultLocationUrl"] = fileUrls;
    
    data["beginTime"] = $("#beginTime").val() + " " +  $("#beginTimeToTime").val();
    data["endTime"] = $("#endTime").val() + " " +  $("#endTimeToTime").val();
    data["state"] = 9;
    return data;
}


$("#beginTime").change(function(){
	
	var beginTime = $("#beginTime").val() + " " +  $("#beginTimeToTime").val();
	var endTime = $("#endTime").val() + " " +  $("#endTimeToTime").val();
	
	var start = new Date(beginTime);
	var end = new Date(endTime);
	
	var num = (end-start)/(1000*3600);

	if(!isNaN(num)){
		$("#maintenanceTime").val(num.toFixed(1) + "小时");
	}else{
		$("#maintenanceTime").val("");
	}
	
    
	
})

$("#beginTimeToTime").change(function(){
	
	var beginTime = $("#beginTime").val() + " " +  $("#beginTimeToTime").val();
	var endTime = $("#endTime").val() + " " +  $("#endTimeToTime").val();
	
	var start = new Date(beginTime);
	var end = new Date(endTime);
	
	var num = (end-start)/(1000*3600);

	if(!isNaN(num)){
		$("#maintenanceTime").val(num.toFixed(1) + "小时");
	}else{
		$("#maintenanceTime").val("");
	}
	
    
	
})

$("#endTime").change(function(){

	var beginTime = $("#beginTime").val() + " " +  $("#beginTimeToTime").val();
	var endTime = $("#endTime").val() + " " +  $("#endTimeToTime").val();
	
	var start = new Date(beginTime);
	var end = new Date(endTime);
	
	var num = (end-start)/(1000*3600);
	
	if(!isNaN(num)){
		$("#maintenanceTime").val(num.toFixed(1) + "小时");
	}else{
		$("#maintenanceTime").val("");
	}

})

$("#endTimeToTime").change(function(){

	var beginTime = $("#beginTime").val() + " " +  $("#beginTimeToTime").val();
	var endTime = $("#endTime").val() + " " +  $("#endTimeToTime").val();
	
	var start = new Date(beginTime);
	var end = new Date(endTime);
	
	var num = (end-start)/(1000*3600);
	
	if(!isNaN(num)){
		$("#maintenanceTime").val(num.toFixed(1) + "小时");
	}else{
		$("#maintenanceTime").val("");
	}

})