$(document).ready(function() {
    var Request = new Object();
    Request = GetRequest();
    var id = Request['id'];
    query(id);
});
function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}
function query(id) {
    $("#id").val(id);
    $.ajax({
        url: "../line/getLineByid.do",
        type: "POST",
        dataType: "json",
        data: {
            "id": Number(id)
        },
        success: function (result) {
            if (result.ret == '1') {
                $('#id').val(id);
                $('#beltlineName').val(result.data.beltlineName);
                $('#beltlineDescription').val(result.data.beltlineDescription);
            } else {
                var error = "";
                for (var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                alert(error);
            }
        }
    });
}

//点击保存按钮
function save(){
    var id = $("#id").val(),
        beltlineName = $("#beltlineName").val(),
        beltlineDescription = $("#beltlineDescription").val();
    if (isNullorEmpty(beltlineName)) {
        layer.alert("生产线名称不能为空");
        return;
    }
    $.ajax({
        url: "../line/updateLine.do",
        type: "POST",
        dataType: "json",
        data: {
            "id":Number(id),
            "beltlineName":beltlineName,
            "beltlineDescription":beltlineDescription
        },
        success: function (result) {
            if (result.ret == '1') {
                layer.msg("保存成功");
                setTimeout(function(){
                    window.location.href = "produce-list.html";
                }, 2000 );
            } else {
                var error = "";
                for (var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                alert(error);
            }
        }
    });
}
//取消
function layer_close() {
    window.location.href = "produce-list.html";
}