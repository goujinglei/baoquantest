$(document).ready(function() {
    var Request = new Object();
    Request = GetRequest();
    var id = Request['id'];
    query(id);
});
function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}
function query(id) {
    //设置默认时间
    $("#reportReapirTime1").val(getNowDate());
    $("#reportReapirTime2").val(getNowTime());
    $.ajax({
        url: "../equip/getEqBylineID.do",
        type: "POST",
        dataType: "json",
        data: {
            "lineId": Number(id)
        },
        async:false,
        success: function (result) {
            if (result.ret == '1') {
                var eqList = result.data;
                for (var i = 0; i < eqList.length; i++) {
                    //TODO 样式变更
                    var radio=document.createElement('input');
                    radio.type="radio";
                    radio.name="equip";
                    radio.value=eqList[i].id;
                    radio.checked="";
                    radio.style="width:80px;";
                    $('#eqs').append(radio).append(eqList[i].equipName);
                }
            } else {
                var error = "";
                for (var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });
}
function save() {
    var reportReapirTime = $("#reportReapirTime1").val()+" "+$("#reportReapirTime2").val(),
        description = $("#description").val();
    var list= $('input:radio[name="equip"]:checked').val();
    if(list==null){
        layer.alert("请选择要报修的设备!");
        return;
    }
    if(isNullorEmpty(description)){
        layer.alert("故障描述不能为空!");
        return;
    }
    // 获取选中radio的value
    var eqId="";
    var obj = document.getElementsByName("equip");
    for (var i = 0; i < obj.length; i++) {
        if (obj[i].checked == true) {
            eqId = obj[i].value;
            break;
        }
    }
    var repairInput = {
        detailId:Number(eqId),
        reportRepairTime:reportReapirTime,
        appearance:description
    };
    var dataJson = JSON.stringify(repairInput);
    $.ajax({
        url: "../repair/addRepair.do",
        type: 'POST',
        data: dataJson,
        dataType : "json",
        async:false,
        contentType: "application/json; charset=utf-8",
        beforeSend: function (XMLHttpRequest) {

        },
        complete: function (XMLHttpRequest, textStatus) {

        },
        success: function(result) {
            if(result.ret == '1') {
                layer.msg("报修成功");
                setTimeout(function(){
                    window.location.href="serviceman.html";
                }, 2000 );
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message + "<br\>");
                }
                if(error != "") {
                    layer.msg(error);
                }
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("error");
        }
    });
}