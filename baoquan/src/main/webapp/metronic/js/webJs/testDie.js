var deId;
$(document).ready(function() {
	
    var Request = new Object();
    Request = GetRequest();
    deId = Request['id'];
	
	
    $.ajax({
        url: "../login/getUserSession.do",
        type: "POST",
        dataType: "json",
        data:{},
        async:false,
        success: function(result) {
            if(result.ret == '1') {
                var a = result.data.username;
                var b = result.data.password;
                var c = result.data.userType;
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });

    query();
});
function query() {
	
    $.ajax({
        url: "../testMould/getTestMouldList.do?deId=" + deId,
        type: "GET",
        success: function(result) {
            if(result.ret == '1') {
                $("#mouleList").html("");
                var mouleList = result.data.TestMouldList;
                for(var i = 0; i < mouleList.length; i++) {
                    var data = mouleList[i];
                    var div  = "<tr class='text-c'>" +
			                        "<td>" + (i+1) + "</td>" +
			                        "<td>"+ data.predictedTestMouldTime +"</td>" +
			                        "<td>"+ data.testMouldStartTime +"</td>" +
			                        "<td>" + data.testMouldEndTime + "</td>" +
			                        "<td>" + resultStr(data.testMouldResult) + "</td>" +
			                        "<td>" + data.remarks + "</td>" +             
			                        "</td>"+
			                   "</tr>";
                    $("#mouleList").append(div);
                }
                
                //判断是否有未完成的记录
                addView(result.data.TestMould);
                
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });

}


function editMould(id){
    window.location.href="editMould.html?id="+id;
}


function saveTestMoule(){
    //验证表单
	var predictedTestMouldTime = $("#predictedTestMouldTime").val().replace("T"," ");
	if(predictedTestMouldTime == ""){
		layer.msg("预计试模时间不能为空");
		return;
	}
	var testMouldStartTime = $("#testMouldStartTime").val().replace("T"," ");
	var testMouldResult = $("input:radio[name='testMouldResult']:checked").val();
	var remarks = $("#remarks").val();
	var id = $("#id").val();
	if(id == ""){
		id = 0;
	}
	
	var testMould = {
			id: id,
			deId: deId,
			predictedTestMouldTime: predictedTestMouldTime,
			testMouldStartTime: testMouldStartTime,
			testMouldResult: testMouldResult,
			remarks: remarks
	}
	
    $.ajax({
  		url: '../testMould/updateTestMould.do',
  		type: 'POST',
  		data: testMould,
        dataType : "json",
        async:false,
  		beforeSend: function (XMLHttpRequest) {
            $("#save").attr('disabled',false);
        },
        complete: function (XMLHttpRequest, textStatus) {
        	$("#save").removeAttr('disabled');
        },
  		success: function(result) {
  			if(result.ret == '1') {
                layer.msg("保存成功");
                query();
  			} else {
  				  
  				var error = "";
  				for(var i = 0; i < result.data.length; i++) {
  					error += (result.data[i].message + "<br\>");
  				}
  				if(error != "") {
  					layer.msg(error);
  					flag = error;
  				}				
  			}   
  		},
  		error: function(XMLHttpRequest, textStatus, errorThrown) {
  			alert("error");
  		}

  	});
}

function delMould(id) {
    $.ajax({
        url: "../equip/deleteEq.do",
        type: "POST",
        dataType: "json",
        data: {
            "id": id
        },
        success: function (result) {
            if (result.ret == '1') {
            } else {
                var error = "";
                for (var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });
    window.location.href="equipment-list.html"
}

function resultStr(data){
	var dataStr = "";
	if(data == 1){
		dataStr = "失败";
	}else if(data == 2){
		dataStr = "成功";
	}
	return dataStr;
}

function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}

//判断时候有未完成的记录
function addView(data){
	if(data == null){
		$("#addTestMould").show();
		$("#add").hide();
		$("#predictedTestMouldTime").val("");
		$("#testMouldStartTime").val("");
		$("#remarks").val("");
		$("#id").val("");
		$("input:radio[name='testMouldResult']").removeAttr("checked");
		
		
	}else{
		$("#addTestMould").hide();
		$("#add").show();
		
	    var predictedTestMouldTime = data.predictedTestMouldTime.replace(" ", "T");
		$("#predictedTestMouldTime").val(predictedTestMouldTime);
		
		if(data.testMouldStartTime != null && data.testMouldStartTime != ""){
			var testMouldStartTime = data.testMouldStartTime.replace(" ", "T");			
			$("#testMouldStartTime").val(testMouldStartTime);
		}

		$("input:radio[value='"+ data.testMouldResult +"']").attr('checked','true');
		$("#remarks").val(data.remarks);		
		$("#id").val(data.id);
	}
}

function testMould_add(){
	$("#add").show();
}