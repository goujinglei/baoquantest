var id;
var userType;
$(document).ready(function() {
    var Request = new Object();
    Request = GetRequest();
    id = Request['id'];
    userType = Request['userType'];

    if(userType == 2){
    	$("#jdsj").hide();
    	$("#wxks").hide();
    	$("#bqxwxsc").hide();
    }else if(userType == 5|| userType == 6){
    	$("#scxwxsc").hide();
    	
    }
    

	//页面赋值
    $.ajax({
        url: "../mould/selectRepairMouldByConfirm.do",
        type: "POST",
        dataType: "json",
        data:{"id": id},
        async:false,
        success: function(result) {
            if(result.ret == '1') {
               var data = result.data.MouldRepair;
               var reportdate = data.reportRepairTime.split(" ");
               $("#reportRepairDate").val(reportdate[0]);
               $("#reportRepairTime").val(reportdate[1]);
               
			   var beginTime = data.beginTime.split(" ");
			   $("#beginTime").val(beginTime[0]);
			   $("#beginTimeToTime").val(beginTime[1]);
			   
			   var endTime = data.endTime.split(" ");
			   $("#endTime").val(endTime[0]);
			   $("#endTimeToTime").val(endTime[1]);
			   
			   var orderDate = data.orderTime.split(" ");
               $("#orderTime").val(orderDate[0]);
               $("#orderTimeToTime").val(orderDate[1]);
               
               
               $("#applicant").val(data.applicant);
               $("#figureNumber").val(data.figureNumber);
               $("#model").val(data.model);
               $("#phenomenalDescription").val(data.phenomenalDescription);
			   $("input:radio[value='"+ data.failurePeriod +"']").attr('checked','true');
			   $("#id").val(data.id);
			   $("#reason").val(data.reason);
			   $("#emergencyDisposal").val(data.emergencyDisposal);
			   $("#reId").val(data.reId);
			   $("#permanentGame").val(data.permanentGame);
			   
			   //给维修用时累计赋值
			   maintenanceTimeView(data.beginTime, data.endTime);
			   
			   //生产线统计维修时间
			   maintenanceTimeSC(data.reportRepairTime, data.endTime);
			   
               //追加图片显示
               if(data.faultLocationUrl != null && data.faultLocationUrl !=""){
            	   pictureView(data.faultLocationUrl);
               }
			   
			   
               //确认部分
			   $("#mouldUser").text(data.mouldUser || "");
			   $("#shiftLeader").text(data.shiftLeader || "");
			   $("#preservationDepartment").text(data.preservationDepartment || "");
			   $("#preservationSectionChief").text(data.preservationSectionChief || "");
			   
			   $("#confirmationTime").text(data.confirmationTime || "");
			   $("#preDepChiefConTime").text(data.preDepChiefConTime || "");
			   $("#preSecChiefConTime").text(data.preSecChiefConTime || "");
			   
			   //表格赋值
			   textMouleTable(result.data.TestMouldList);
			   
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });
    
});


//点击取消按钮
function layer_close(){
    window.location.href = "equipment-list.html";
}

//点击提交
function save() {

	//验证
	var formValidation = fv();
	if(!formValidation){
		return;
	}

	//取值
	var data = getValue();

      $.ajax({
  		url: '../mould/updateRepairMould.do',
  		type: 'POST',
  		data: data,
        dataType : "json",
        async:false,
  		beforeSend: function (XMLHttpRequest) {

          },
          complete: function (XMLHttpRequest, textStatus) {

          },
  		success: function(result) {
  			if(result.ret == '1') {
                layer.msg("保存成功");
				setTimeout(function(){
					window.location.href= "serviceman.html"
				}, 2000 );
  			} else {
  				  
  				var error = "";
  				for(var i = 0; i < result.data.length; i++) {
  					error += (result.data[i].message + "<br\>");
  				}
  				if(error != "") {
  					layer.msg(error);
  					flag = error;
  				}				
  			}   
  		},
  		error: function(XMLHttpRequest, textStatus, errorThrown) {
  			alert("error");
  		}

  	});

}

function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}


function custom_close(){
	window.location.href= "serviceman.html";
}

function maintenanceTimeView(beginTime, endTime){
	var start = new Date(beginTime);
	var end = new Date(endTime);
	
	var num = (end-start)/(1000*60);

	if(!isNaN(num)){
		$("#maintenanceTime").val(num + "分钟" );
	}else{
		$("#maintenanceTime").val("");
	}
}

function maintenanceTimeSC(reportRepairTime, endTime){
	var start = new Date(reportRepairTime);
	var end = new Date(endTime);
	
	var num = (end-start)/(1000*60);

	if(!isNaN(num)){
		$("#maintenanceTimeSC").val(num + "分钟");
	}else{
		$("#maintenanceTimeSC").val("");
	}
}


function pictureView(url){
	var urls = url.split(",");
	if(urls != "" && urls.length > 0){
		for(var i=0; i < urls.length; i++){
			var picturUrl = urls[i];
			var html = "<div class='row cl'>" +
					   	  "<a href='#' onclick=\"view('"+ picturUrl +"')\" style='margin-left:50px;width:100px;' >图片"+ (i+1) +"&nbsp;&nbsp;&nbsp;&nbsp;点击查看</a>" +	
					   "</div>";
			$("#pictureView").append(html);
		}
	}

}

function view(url){

	window.open(url);
}

function allConfirm(){
	var flag = false;
	
    $.ajax({
  		url: '../mould/mouldRepairByMonitorConfirm.do?id=' + id,
  		type: 'GET',
        async:false,
  		beforeSend: function (XMLHttpRequest) {

        },
        complete: function (XMLHttpRequest, textStatus) {

        },
  		success: function(result) {
  			if(result.ret == '1') {
  				flag = true;

  			} else {
  				  
  				var error = "";
  				for(var i = 0; i < result.data.length; i++) {
  					error += (result.data[i].message + "<br\>");
  				}
  				if(error != "") {
  					layer.msg(error);
  					flag = error;
  				}				
  			}   
  		},
  		error: function(XMLHttpRequest, textStatus, errorThrown) {
  			alert("error");
  		}

  	});
    
    return flag;
}

//试模记录表格
function textMouleTable(mouleList){
    for(var i = 0; i < mouleList.length; i++) {
        var data = mouleList[i];
        var div  = "<tr class='text-c'>" +
                        "<td>" + (i+1) + "</td>" +
                        "<td>"+ data.predictedTestMouldTime +"</td>" +
                        "<td>"+ data.testMouldStartTime +"</td>" +
                        "<td>" + data.testMouldEndTime + "</td>" +
                        "<td>" + resultStr(data.testMouldResult) + "</td>" +
                        "<td>" + data.remarks + "</td>" +             
                        "</td>"+
                   "</tr>";
        $("#mouleList").append(div);
    }
}

function resultStr(data){
	var dataStr = "";
	if(data == 1){
		dataStr = "失败";
	}else if(data == 2){
		dataStr = "成功";
	}
	return dataStr;
}