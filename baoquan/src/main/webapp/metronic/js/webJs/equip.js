$(document).ready(function() {
    $.ajax({
        url: "../login/getUserSession.do",
        type: "POST",
        dataType: "json",
        data:{},
        async:false,
        success: function(result) {
            if(result.ret == '1') {
                var a = result.data.username;
                var b = result.data.password;
                var c = result.data.userType;
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });

    $.ajax({
        url: '../mould/initCombox.do',
        type: 'GET',
        dataType: 'JSON',
        async:false,
        success: function(result) {
            if(result.ret == '1') {
                initComBox(result.data, "factory");

            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            layer.alert("error");
        }
    });
    query();
});
function query() {
    var PageCount;  //总页数，通过ajax获取
    var pageIndex = 0;     //页面索引初始值
    var pageSize = 10;     //每页显示条数初始化，修改显示条数，修改这里即可
    $.ajax({
        url: "../equip/getEquipCount.do",
        type: "POST",
        dataType: "json",
        data:{},
        async:false,
        success: function(result) {
            if(result.ret == '1') {
                PageCount=result.data;
            } else {
                PageCount = 1;
            }
        }
    });
    InitTable(0); //Load事件，初始化表格数据，页面索引为0（第一页）
    //分页，PageCount是总条目数，这是必选参数，其它参数都是可选
    $("#Pagination").pagination(PageCount, {
        callback: PageCallback, //PageCallback() 为翻页调用次函数。
        prev_text: "« 上一页",
        next_text: "下一页 »",
        items_per_page: pageSize,
        num_edge_entries: 2, //两侧首尾分页条目数
        num_display_entries: 6, //连续分页主体部分分页条目数
        current_page: pageIndex, //当前页索引
    });
    //翻页调用
    function PageCallback(index, jq) {
        InitTable(index);
    }
}
function InitTable(pageIndex) {
    var pageSize=10;
    var beginIndex = pageIndex*pageSize;
    $.ajax({
        url: "../equip/getEquip.do",
        type: "POST",
        dataType: "json",
        data:{
            "beginIndex":beginIndex,
            "pageSize":pageSize
        },
        success: function(result) {
            if(result.ret == '1') {
                $("#equipList").html("");
                var equipList = result.data;
                for(var i = 0; i < equipList.length; i++) {
                    var data = equipList[i];
                    var j = i+1+beginIndex;
                    var div  = "<tr class='text-c'>" +
                        "<td>" + j + "</td>" +
                        "<td style='display: none'>" + data.id + "</td>" +
                        "<td>"+ data.equipName +"</td>" +
                        "<td>"+ data.equipDescription +"</td>" +
                        "<td>" + data.useBeginTime + "</td>" +
                        "<td>" + data.yearsLimit + "</td>" +
                        "<td>" + data.equipModel + "</td>" +
                        "<td>" + data.standard + "</td>" +
                        "<td><a class='btn btn-primary radius' style='margin-right:10px'"+
                        "onclick='editEq(" + data.id + ")'>修改</a>"+
                        "<a class='btn btn-primary radius' style='margin-right:10px'"+
                        "onclick='delEq(" + data.id + ")'>删除</a>"+
                        "<a class='btn btn-primary radius'"+
                        "onclick='eqSee(" + data.id + ")'>详情</a>"+
                        "</td>"+
                        "</tr>";
                    $("#equipList").append(div);
                }
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                // if(error != "") {
                //     layer.msg(error);
                //     window.location.href="login.html";
                // }
                //layer.alert(error);
            }
        }
    });
}

//修改
function editEq(id){
    window.location.href="editequip.html?id="+id;
}

// 点击检索
function search() {
    var equipName = $("#equipName").val(),
        equipDescription = $("#equipDescription").val(),
        useBeginTime = $("#useBeginTime").val(),
        equipModel = $("#equipModel").val(),
        standard = $("#standard").val();
    var factory = $("#factory option:selected").val();
    var PageCount;  //总页数，通过ajax获取
    var pageIndex = 0;     //页面索引初始值
    var pageSize = 10;     //每页显示条数初始化，修改显示条数，修改这里即可
    $.ajax({
        url: "../equip/getEquipByFrimCount.do",
        type: "POST",
        dataType: "json",
        data: {
            "equipName": equipName,
            "equipDescription": equipDescription,
            "useBeginTime": useBeginTime,
            "equipModel": equipModel,
            "standard": standard,
            "factory":Number(factory)
        },
        async: false,
        success: function (result) {
            if (result.ret == '1') {
                PageCount = result.data;
            } else {
                PageCount = 1;
            }
        }
    });
    InitTableNum(0, equipName, equipDescription,useBeginTime,equipModel,standard,factory); //Load事件，初始化表格数据，页面索引为0（第一页）
    //分页，PageCount是总条目数，这是必选参数，其它参数都是可选
    $("#Pagination").pagination(PageCount, {
        callback: PageCallback, //PageCallback() 为翻页调用次函数。
        prev_text: "« 上一页",
        next_text: "下一页 »",
        items_per_page: pageSize,
        num_edge_entries: 2, //两侧首尾分页条目数
        num_display_entries: 6, //连续分页主体部分分页条目数
        current_page: pageIndex, //当前页索引
    });

    //翻页调用
    function PageCallback(index, jq) {
        InitTableNum(index, equipName, equipDescription,useBeginTime,equipModel,standard,factory);
    }
}
function InitTableNum(pageIndex,equipName, equipDescription,useBeginTime,equipModel,standard,factory) {
    var pageSize=10;
    var beginIndex = pageIndex*pageSize;
    $.ajax({
        url: "../equip/getEquipByFrim.do",
        type: "POST",
        dataType: "json",
        data: {
            "equipName": equipName,
            "equipDescription": equipDescription,
            "useBeginTime": useBeginTime,
            "equipModel": equipModel,
            "standard": standard,
            "factory":Number(factory),
            "beginIndex":beginIndex,
            "pageSize":pageSize
        },
        success: function (result) {
            if (result.ret == '1') {
                $("#equipList").html("");
                var equipList = result.data;
                for (var i = 0; i < equipList.length; i++) {
                    var data = equipList[i];
                    var j = i + 1;
                    var div  = "<tr class='text-c'>" +
                        "<td>" + j + "</td>" +
                        "<td style='display: none'>" + data.id + "</td>" +
                        "<td>"+ data.equipName +"</td>" +
                        "<td>"+ data.equipDescription +"</td>" +
                        "<td>" + data.useBeginTime + "</td>" +
                        "<td>" + data.yearsLimit + "</td>" +
                        "<td>" + data.equipModel + "</td>" +
                        "<td>" + data.standard + "</td>" +
                        "<td><a class='btn btn-primary radius'"+
                        "onclick='editEq(" + data.id + ")'>修改</a>"+
                        "<a class='btn btn-primary radius'"+
                        "onclick='delEq(" + data.id + ")'>删除</a>"+
                        "<a class='btn btn-primary radius'"+
                        "onclick='eqSee(" + data.id + ")'>详情</a>"+
                        "</td>"+
                        "</tr>";
                    $("#equipList").append(div);
                }
            } else {
                var error = "";
                for (var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });
}

//详情
function eqSee(id) {
    window.location.href="equipsee.html?id="+id;
}

//添加
function equip_add(){
    window.location.href="addequip.html";
}

//删除
function delEq(id) {
    $.ajax({
        url: "../equip/deleteEq.do",
        type: "POST",
        dataType: "json",
        data: {
            "id": id
        },
        success: function (result) {
            if (result.ret == '1') {
                layer.msg("删除成功");
                setTimeout(function(){
                    window.location.href="equipment-list.html"
                }, 2000 );
            } else {
                var error = "";
                for (var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });

}

function initComBox(selectList, htmlId){

    var optionHtml = "<option value='9'>请选择</option>";
    $(selectList).each(function(i, e) {

        optionHtml += '<option value="' + e.id + '">' + e.name + '</option>';
    });
    $("#" + htmlId).append(optionHtml);

}
