$(document).ready(function() {
    var Request = new Object();
    Request = GetRequest();
    var id = Request['id'];
    var cycle = Request['cycle'];
    var mouldId = Request['mouldId'];
    $("#id").val(id);
    $("#mouldId").val(mouldId);
    $("#cycle").val(cycle);
    $.ajax({
        url: "../login/getUserSession.do",
        type: "POST",
        dataType: "json",
        data:{},
        async:false,
        success: function(result) {
            if(result.ret == '1') {
                $("#username").val(result.data.username);
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });
    query(id,mouldId,cycle);
});
function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}
function query(id,mouldId,cycle) {

    $.ajax({
        url: "../mould/selectMouldById.do",
        type: "POST",
        dataType: "json",
        data: {
            "id": Number(mouldId)
        },
        async:false,
        success: function (result) {
            if (result.ret == '1') {
                var data = result.data;
                $("#vehicleType").val(data.vehicleType);
                $("#figureNumber").val(data.figureNumber);
            } else {
                var error = "";
                for (var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                alert(error);
            }
        }
    });

    var cycle = $("#cycle").val();
    $.ajax({
        url: "../spotCheck/getCheck.do",
        type: "POST",
        dataType: "json",
        data:{
            "spotDetailId":id,
            "principalNumbe":Number(mouldId),
            "spotInterval":cycle,
            "type":2
        },
        async:false,
        success: function(result) {
            if(result.ret == '1') {
                $("#checkList").html("");
                var checkList = result.data;
                for(var i = 0; i < checkList.length; i++) {
                    var str = "isUnusual"+i;
                    var str1 = "isUpdate"+i;
                    var data = checkList[i];
                    var j = i+1;
                    var div  = "<tr class='text-c'>" +
                        "<td>" + j + "</td>" +
                        "<td>"+ data.spotPosition +"</td>" +
                        "<td>"+data.checkProject +"</td>" +
                        "<td>" + data.checkMethod + "</td>" +
                        "<td><select name='isUnusual' id='"+str+"' class='select'><option  value='9'>请选择</option>"+
                        "<option  value='1'>有</option>"+
                        "<option  value='2'>无</option>"+
                        "</select>"+"</td>" +
                        "<td contentEditable='true' >" + checkunusualText(data.unusualText) + "</td>" +
                        "<td><select name='isUpdate' id='"+str1+"' class='select'><option  value='9'>请选择</option>"+
                        "<option  value='1'>有</option>"+
                        "<option  value='2'>无</option>"+
                        "</select>"+"</td>" +
                        "<td contentEditable='true'>" + checkupdateText(data.updateText) +"</td>" +
                        "<td>" + checkspotTime(data.spotTime,i) +"</td>" +
                        "<td>" + data.remarks +"</td>" +
                        "<td>" + pictureView(data.prictureUrl) +"</td>" +
                        "<td style='display: none'>"+ data.id +"</td>" +
                        "</tr>";
                    $("#checkList").append(div);

                    var all_options = document.getElementById(str).options;
                    for (var p=0; p<all_options.length; p++){
                        if (Number(all_options[p].value) == data.isUnusual) {
                            all_options[p].selected = true;
                        }
                    }
                    var all_options1 = document.getElementById(str1).options;
                    for (var q=0; q<all_options1.length; q++){
                        if (Number(all_options1[q].value) == data.isUpdate) {
                            all_options1[q].selected = true;
                        }
                    }
                }
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
            }
        }
    });
}

function checkunusualText(unusualText) {
    if(unusualText==null){
        return "";
    }else{
        return unusualText;
    }
}

function checkupdateText(updateText) {
    if(updateText==null){
        return "";
    }else{
        return updateText;
    }
}

function checkspotTime(spotTime,j) {
    var str = "spotTime"+j;
    if(spotTime==null){
        return "<input type='date' id='"+str+"' name='spotTime' class='input-text'>";
    }else{
        return "<input type='date' id='"+str+"' name='spotTime' class='input-text' value='"+spotTime +"'>";
    }
}

function toInterval(interval) {
    if(interval=='1'){
        return "月";
    }else if(interval=='3'){
        return "季度";
    }else if(interval=='6'){
        return "半年";
    }else if(interval=='12'){
        return "年";
    }
}

function initComBox(selectList, htmlId){

    var optionHtml = "<option value='9'>请选择</option>";
    $(selectList).each(function(i, e) {

        optionHtml += '<option value="' + e.id + '">' + e.name + '</option>';
    });
    $("#" + htmlId).append(optionHtml);

}

function save() {
    var jdList = [];
    var flag = true;
    $("#checkList tr").each(function (i) {
        var jd = {};
        $(this).children('td').each(function (j) {
            if (j == 4) {
                var str = "#isUnusual"+i+" option:selected";
                var isUnusual = $(str).val();
                jd["isUnusual"] = Number(isUnusual);
                if(isUnusual=="9"){
                    flag = false;
                }
            }
            else if (j == 5) {
                var unusualText = $(this).text();
                jd["unusualText"] = unusualText;
            }
            else if (j == 6) {
                var str = "#isUpdate"+i+" option:selected";
                var isUpdate = $(str).val();
                jd["isUpdate"] = Number(isUpdate);
                if(isUpdate=="9"){
                    flag = false;
                }
            }
            else if (j == 7) {
                var updateText = $(this).text();
                jd["updateText"] = updateText;
            }
            else if (j == 8) {
                var str = "#spotTime"+i;
                var spotTime = $(str).val();
                jd["spotTime"] = spotTime;
                if(spotTime==null||spotTime==""){
                    flag = false;
                }
            }
            else if (j == 11) {
                var id = $(this).text();
                jd["id"] = Number(id);
            }
        });
        jdList.push(jd);
    });

    var jdListJson = JSON.stringify(jdList);

    $.ajax({
        url: '../spotCheck/update.do',
        type: 'POST',
        data: jdListJson,
        async:false,
        contentType: "application/json; charset=utf-8",
        beforeSend: function (XMLHttpRequest) {

        },
        complete: function (XMLHttpRequest, textStatus) {

        },
        success: function(result) {
            if(result.ret == '1') {
                layer.msg("保存成功");
                setTimeout(function(){
                    window.location.href="spotTask.html"
                }, 2000 );
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message + "<br\>");
                }
                if(error != "") {
                    layer.msg(error);
                }
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("error");
        }
    });

    //更新状态
    if(flag) {
        $.ajax({
            url: "../spotDetail/updateState.do",
            type: "POST",
            dataType: "json",
            async: false,
            data: {
                "id": Number($("#id").val())
            },
            success: function (result) {
                if (result.ret == '1') {
                } else {
                    var error = "";
                    for (var i = 0; i < result.data.length; i++) {
                        error += (result.data[i].message);
                    }
                    layer.alert(error);
                }
            }
        });
    }
}

function pictureView(url){
    if(url==""||url==null){
        return "";
    }
    var urls = url.split(",");
    if(urls != "" && urls.length > 0){
        for(var i=0; i < urls.length; i++){
            var picturUrl = urls[i];
            var html = "<p>" +
                "<a href='#' onclick=\"view('"+ picturUrl +"')\" style='width:100px;' >图片"+ (i+1) +"&nbsp;点击查看</a>" +
                "<input type='text' name='fileUrls' value='"+ picturUrl +"' style='display:none'/>" +
                "</p>";
            return html;
        }
    }
}
function view(url){
    window.open(url);
}