$(document).ready(function() {
    query();
});
function query() {
    $.ajax({
        url: "../line/getLine.do",
        type: "POST",
        dataType: "json",
        data:{
            "beginIndex":0,
            "pageSize":10000
        },
        success: function(result) {
            if(result.ret == '1') {
                $("#lineAdd").html("");
                var lineList = result.data;
                for(var i = 0; i < lineList.length; i++) {
                    var data = lineList[i];
                    var div  = "<div class='col-6 col-sm-4 col-lg-2'>" +
                        "<a onclick='toSpot("+data.id+")'>" +
                        "<div class='card'>"+
                        "<div class='card-body p-3 text-center'>" +
                        "<div class='h1'>"+data.beltlineName+"</div>"+
                        "<div class='text-muted mb-4'>"+data.beltlineDescription+"</div>"+
                        "</div></div></a></div>";
                    $("#lineAdd").append(div);
                }
            } else {
                var error = "";
                for(var i = 0; i < result.data.length; i++) {
                    error += (result.data[i].message);
                }
                layer.alert(error);
            }
        }
    });
}
function toSpot(id) {
    window.location.href="spot.html?id="+id;
}
