package com.andon.bean;

import java.io.Serializable;

public class UserBaseInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String username;
    private String password;
    private int userType;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }
}
