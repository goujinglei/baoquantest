package com.andon.bean;

import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private String username;
    private String password;
    private int userType;
    private String worksection;
    private String system;
    private String classment;
    private String ce1;
    private String ceshi2;

    public String getWorksection() {
        return worksection;
    }

    public void setWorksection(String worksection) {
        this.worksection = worksection;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getClassment() {
        return classment;
    }

    public void setClassment(String classment) {
        this.classment = classment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }
}
