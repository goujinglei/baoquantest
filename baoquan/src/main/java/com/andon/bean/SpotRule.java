package com.andon.bean;

public class SpotRule extends BaseEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	
	private String groupKey;
	
	private String spotInspection;  //点检部位（模具）  
	
	private String spotPosition;  //点检项目

	private String checkProject;  //点检基准
	
	private String checkMethod;  //检查方法
	
	private int type;  //类型（1.设备  2.模具）
	
	private String classification;  //设备或模具具体分类（设备存类型，模具存id）
	
	private String cycle;  //周期（设备存1、3、6、12， 模具存次数）
	
	private String remarks;  //备注
	
	private String prictureUrl;  //图片路径

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getSpotPosition() {
		return spotPosition;
	}

	public void setSpotPosition(String spotPosition) {
		this.spotPosition = spotPosition;
	}

	public String getCheckProject() {
		return checkProject;
	}

	public void setCheckProject(String checkProject) {
		this.checkProject = checkProject;
	}

	public String getCheckMethod() {
		return checkMethod;
	}

	public void setCheckMethod(String checkMethod) {
		this.checkMethod = checkMethod;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPrictureUrl() {
		return prictureUrl;
	}

	public void setPrictureUrl(String prictureUrl) {
		this.prictureUrl = prictureUrl;
	}

	public String getGroupKey() {
		return groupKey;
	}

	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}

	public String getSpotInspection() {
		return spotInspection;
	}

	public void setSpotInspection(String spotInspection) {
		this.spotInspection = spotInspection;
	}

}
