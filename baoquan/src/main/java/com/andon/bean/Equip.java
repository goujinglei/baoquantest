package com.andon.bean;

import java.io.Serializable;

public class Equip extends BaseEntity  implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private int lineId;
    private String assetNum;
    private String equipNum;
    private String equipName;
    private String equipDescription;
    private String subsidiaryEq;
    private int isBottleneck;
    private String manufactory;
    private String country;
    private String manuYear;
    private String useBeginTime;
    private String yearsLimit;
    private String equipModel;
    private String standard;
    private int factory;

    public int getFactory() {
        return factory;
    }

    public void setFactory(int factory) {
        this.factory = factory;
    }

    public String getAssetNum() {
        return assetNum;
    }

    public void setAssetNum(String assetNum) {
        this.assetNum = assetNum;
    }

    public String getEquipNum() {
        return equipNum;
    }

    public void setEquipNum(String equipNum) {
        this.equipNum = equipNum;
    }

    public String getSubsidiaryEq() {
        return subsidiaryEq;
    }

    public void setSubsidiaryEq(String subsidiaryEq) {
        this.subsidiaryEq = subsidiaryEq;
    }

    public int getIsBottleneck() {
        return isBottleneck;
    }

    public void setIsBottleneck(int isBottleneck) {
        this.isBottleneck = isBottleneck;
    }

    public String getManufactory() {
        return manufactory;
    }

    public void setManufactory(String manufactory) {
        this.manufactory = manufactory;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getManuYear() {
        return manuYear;
    }

    public void setManuYear(String manuYear) {
        this.manuYear = manuYear;
    }

    public int getLineId() {
        return lineId;
    }

    public void setLineId(int lineId) {
        this.lineId = lineId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEquipName() {
        return equipName;
    }

    public void setEquipName(String equipName) {
        this.equipName = equipName;
    }

    public String getEquipDescription() {
        return equipDescription;
    }

    public void setEquipDescription(String equipDescription) {
        this.equipDescription = equipDescription;
    }

    public String getUseBeginTime() {
        return useBeginTime;
    }

    public void setUseBeginTime(String useBeginTime) {
        this.useBeginTime = useBeginTime;
    }

    public String getYearsLimit() {
        return yearsLimit;
    }

    public void setYearsLimit(String yearsLimit) {
        this.yearsLimit = yearsLimit;
    }

    public String getEquipModel() {
        return equipModel;
    }

    public void setEquipModel(String equipModel) {
        this.equipModel = equipModel;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }
}
