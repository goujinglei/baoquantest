package com.andon.bean;

import java.io.Serializable;

public class SpotDetail extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private int taskId;
    private int type;
    private int detailId;
    private int state;
    private int confirmState;
    private String typeConfirmMan;
    private String typeConfirmTime;
    private String classConfirmMan;
    private String classConfirmTime;
    private String formingConfirmMan;
    private String formingConfirmTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDetailId() {
        return detailId;
    }

    public void setDetailId(int detailId) {
        this.detailId = detailId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getConfirmState() {
        return confirmState;
    }

    public void setConfirmState(int confirmState) {
        this.confirmState = confirmState;
    }

    public String getTypeConfirmMan() {
        return typeConfirmMan;
    }

    public void setTypeConfirmMan(String typeConfirmMan) {
        this.typeConfirmMan = typeConfirmMan;
    }

    public String getTypeConfirmTime() {
        return typeConfirmTime;
    }

    public void setTypeConfirmTime(String typeConfirmTime) {
        this.typeConfirmTime = typeConfirmTime;
    }

    public String getClassConfirmMan() {
        return classConfirmMan;
    }

    public void setClassConfirmMan(String classConfirmMan) {
        this.classConfirmMan = classConfirmMan;
    }

    public String getClassConfirmTime() {
        return classConfirmTime;
    }

    public void setClassConfirmTime(String classConfirmTime) {
        this.classConfirmTime = classConfirmTime;
    }

    public String getFormingConfirmMan() {
        return formingConfirmMan;
    }

    public void setFormingConfirmMan(String formingConfirmMan) {
        this.formingConfirmMan = formingConfirmMan;
    }

    public String getFormingConfirmTime() {
        return formingConfirmTime;
    }

    public void setFormingConfirmTime(String formingConfirmTime) {
        this.formingConfirmTime = formingConfirmTime;
    }
}
