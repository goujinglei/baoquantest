package com.andon.bean;

import java.io.Serializable;

public class Repair extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private int detailId;  //设备或模具主表
    private int type;      //1设备2模具
    private int state;     //状态
    private String applicant;   //报修人
    private String workMan;     //保全作业者
    private String reportRepairTime;  //报修时间
    private String orderTime;  //接单时间
    private String beginTime;  //维修开始
    private String endTime;    //维修结束
    private String confirmationTime;   //生产线确认维修完成时间
    private String preDepChiefConTime; //保全系长确认时间
    private String preSecChiefConTime; //保全科长确认时间

    public String getWorkMan() {
        return workMan;
    }

    public void setWorkMan(String workMan) {
        this.workMan = workMan;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public int getDetailId() {
        return detailId;
    }

    public void setDetailId(int detailId) {
        this.detailId = detailId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getReportRepairTime() {
        return reportRepairTime;
    }

    public void setReportRepairTime(String reportRepairTime) {
        this.reportRepairTime = reportRepairTime;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

	public String getConfirmationTime() {
		return confirmationTime;
	}

	public void setConfirmationTime(String confirmationTime) {
		this.confirmationTime = confirmationTime;
	}

	public String getPreDepChiefConTime() {
		return preDepChiefConTime;
	}

	public void setPreDepChiefConTime(String preDepChiefConTime) {
		this.preDepChiefConTime = preDepChiefConTime;
	}

	public String getPreSecChiefConTime() {
		return preSecChiefConTime;
	}

	public void setPreSecChiefConTime(String preSecChiefConTime) {
		this.preSecChiefConTime = preSecChiefConTime;
	}
    
    
}
