package com.andon.bean;

import java.io.Serializable;

public class SpotTask extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private int spotType;
    private String spotName;
    private String spotInterval;
    private String beginTime;
    private String endTime;
    private int state;

    //非数据库字段
    private int detailId;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSpotType() {
        return spotType;
    }

    public void setSpotType(int spotType) {
        this.spotType = spotType;
    }

    public String getSpotName() {
        return spotName;
    }

    public void setSpotName(String spotName) {
        this.spotName = spotName;
    }

    public String getSpotInterval() {
        return spotInterval;
    }

    public void setSpotInterval(String spotInterval) {
        this.spotInterval = spotInterval;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

	public int getDetailId() {
		return detailId;
	}

	public void setDetailId(int detailId) {
		this.detailId = detailId;
	}
    
}
