package com.andon.bean.dto;

import com.andon.bean.BaseEntity;

import java.io.Serializable;

public class UpdateEquipRepair  extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private int reId;
    private String equipUseDepartment;
    private String equipUseSystem;
    private String equipUseClass;
    private String equipState;
    private String operator;
    private String safetyDeviceConfirm;
    private String maintainer;
    private String taskConfirmMan;
    private String mainTaskMan;
    private String subTaskMan;
    private String newaddTaskMan;
    private String supplier;
    private int isfirstEpisode;
    private String appearance;
    private String reason;
    private String management;
    private String preventPlan;
    private String saveConfirmChief;
    private String saveConfirmCommander;
    private int faultType;
    private int repairUsetimeType;
    private int isOverNum;
    private String locationUrl;
    private String partsListStr;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReId() {
        return reId;
    }

    public void setReId(int reId) {
        this.reId = reId;
    }

    public String getEquipUseDepartment() {
        return equipUseDepartment;
    }

    public void setEquipUseDepartment(String equipUseDepartment) {
        this.equipUseDepartment = equipUseDepartment;
    }

    public String getEquipUseSystem() {
        return equipUseSystem;
    }

    public void setEquipUseSystem(String equipUseSystem) {
        this.equipUseSystem = equipUseSystem;
    }

    public String getEquipUseClass() {
        return equipUseClass;
    }

    public void setEquipUseClass(String equipUseClass) {
        this.equipUseClass = equipUseClass;
    }

    public String getEquipState() {
        return equipState;
    }

    public void setEquipState(String equipState) {
        this.equipState = equipState;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getSafetyDeviceConfirm() {
        return safetyDeviceConfirm;
    }

    public void setSafetyDeviceConfirm(String safetyDeviceConfirm) {
        this.safetyDeviceConfirm = safetyDeviceConfirm;
    }

    public String getMaintainer() {
        return maintainer;
    }

    public void setMaintainer(String maintainer) {
        this.maintainer = maintainer;
    }

    public String getTaskConfirmMan() {
        return taskConfirmMan;
    }

    public void setTaskConfirmMan(String taskConfirmMan) {
        this.taskConfirmMan = taskConfirmMan;
    }

    public String getMainTaskMan() {
        return mainTaskMan;
    }

    public void setMainTaskMan(String mainTaskMan) {
        this.mainTaskMan = mainTaskMan;
    }

    public String getSubTaskMan() {
        return subTaskMan;
    }

    public void setSubTaskMan(String subTaskMan) {
        this.subTaskMan = subTaskMan;
    }

    public String getNewaddTaskMan() {
        return newaddTaskMan;
    }

    public void setNewaddTaskMan(String newaddTaskMan) {
        this.newaddTaskMan = newaddTaskMan;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public int getIsfirstEpisode() {
        return isfirstEpisode;
    }

    public void setIsfirstEpisode(int isfirstEpisode) {
        this.isfirstEpisode = isfirstEpisode;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getManagement() {
        return management;
    }

    public void setManagement(String management) {
        this.management = management;
    }

    public String getPreventPlan() {
        return preventPlan;
    }

    public void setPreventPlan(String preventPlan) {
        this.preventPlan = preventPlan;
    }

    public String getSaveConfirmChief() {
        return saveConfirmChief;
    }

    public void setSaveConfirmChief(String saveConfirmChief) {
        this.saveConfirmChief = saveConfirmChief;
    }

    public String getSaveConfirmCommander() {
        return saveConfirmCommander;
    }

    public void setSaveConfirmCommander(String saveConfirmCommander) {
        this.saveConfirmCommander = saveConfirmCommander;
    }

    public int getFaultType() {
        return faultType;
    }

    public void setFaultType(int faultType) {
        this.faultType = faultType;
    }

    public int getRepairUsetimeType() {
        return repairUsetimeType;
    }

    public void setRepairUsetimeType(int repairUsetimeType) {
        this.repairUsetimeType = repairUsetimeType;
    }

    public int getIsOverNum() {
        return isOverNum;
    }

    public void setIsOverNum(int isOverNum) {
        this.isOverNum = isOverNum;
    }

    public String getLocationUrl() {
        return locationUrl;
    }

    public void setLocationUrl(String locationUrl) {
        this.locationUrl = locationUrl;
    }

    public String getPartsListStr() {
        return partsListStr;
    }

    public void setPartsListStr(String partsListStr) {
        this.partsListStr = partsListStr;
    }
}
