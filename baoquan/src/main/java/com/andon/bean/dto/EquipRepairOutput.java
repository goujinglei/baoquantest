package com.andon.bean.dto;

import com.andon.bean.BaseEntity;
import com.andon.bean.Parts;

import java.io.Serializable;
import java.util.List;

public class EquipRepairOutput extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private String equipNum;
    private String equipName;
    private String reportRepairTime;
    private String orderTime;
    private String beginTime;
    private String endTime;
    private String maintainer;
    private String mainTaskMan;
    private String subTaskMan;
    private String newaddTaskMan;
    private String operator;
    private String appearance;
    private int isfirstEpisode;
    private String reason;
    private String management;
    private String preventPlan;
    private int faultType;
    private int repairUsetimeType;
    private String worksection;
    private String system;
    private String classment;
    private String supplier;
    private int isOverNum;
    private String saveConfirmCommander;
    private String  saveConfirmChief;
    private String  taskConfirmMan;
    private String  confirmationTime;
    private String  preDepChiefConTime;
    private String  preSecChiefConTime;
    private int state;
    private String locationUrl;

    private List<Parts> partsList;

    public List<Parts> getPartsList() {
        return partsList;
    }

    public void setPartsList(List<Parts> partsList) {
        this.partsList = partsList;
    }

    public String getLocationUrl() {
        return locationUrl;
    }

    public void setLocationUrl(String locationUrl) {
        this.locationUrl = locationUrl;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(String confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    public String getPreDepChiefConTime() {
        return preDepChiefConTime;
    }

    public void setPreDepChiefConTime(String preDepChiefConTime) {
        this.preDepChiefConTime = preDepChiefConTime;
    }

    public String getPreSecChiefConTime() {
        return preSecChiefConTime;
    }

    public void setPreSecChiefConTime(String preSecChiefConTime) {
        this.preSecChiefConTime = preSecChiefConTime;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getTaskConfirmMan() {
        return taskConfirmMan;
    }

    public void setTaskConfirmMan(String taskConfirmMan) {
        this.taskConfirmMan = taskConfirmMan;
    }

    public String getSaveConfirmCommander() {
        return saveConfirmCommander;
    }

    public void setSaveConfirmCommander(String saveConfirmCommander) {
        this.saveConfirmCommander = saveConfirmCommander;
    }

    public String getSaveConfirmChief() {
        return saveConfirmChief;
    }

    public void setSaveConfirmChief(String saveConfirmChief) {
        this.saveConfirmChief = saveConfirmChief;
    }

    public int getIsOverNum() {
        return isOverNum;
    }

    public void setIsOverNum(int isOverNum) {
        this.isOverNum = isOverNum;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getWorksection() {
        return worksection;
    }

    public void setWorksection(String worksection) {
        this.worksection = worksection;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getClassment() {
        return classment;
    }

    public void setClassment(String classment) {
        this.classment = classment;
    }

    public int getFaultType() {
        return faultType;
    }

    public void setFaultType(int faultType) {
        this.faultType = faultType;
    }

    public int getRepairUsetimeType() {
        return repairUsetimeType;
    }

    public void setRepairUsetimeType(int repairUsetimeType) {
        this.repairUsetimeType = repairUsetimeType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getManagement() {
        return management;
    }

    public void setManagement(String management) {
        this.management = management;
    }

    public String getPreventPlan() {
        return preventPlan;
    }

    public void setPreventPlan(String preventPlan) {
        this.preventPlan = preventPlan;
    }

    public int getIsfirstEpisode() {
        return isfirstEpisode;
    }

    public void setIsfirstEpisode(int isfirstEpisode) {
        this.isfirstEpisode = isfirstEpisode;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getMaintainer() {
        return maintainer;
    }

    public void setMaintainer(String maintainer) {
        this.maintainer = maintainer;
    }

    public String getEquipNum() {
        return equipNum;
    }

    public void setEquipNum(String equipNum) {
        this.equipNum = equipNum;
    }

    public String getEquipName() {
        return equipName;
    }

    public void setEquipName(String equipName) {
        this.equipName = equipName;
    }

    public String getReportRepairTime() {
        return reportRepairTime;
    }

    public void setReportRepairTime(String reportRepairTime) {
        this.reportRepairTime = reportRepairTime;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getMainTaskMan() {
        return mainTaskMan;
    }

    public void setMainTaskMan(String mainTaskMan) {
        this.mainTaskMan = mainTaskMan;
    }

    public String getSubTaskMan() {
        return subTaskMan;
    }

    public void setSubTaskMan(String subTaskMan) {
        this.subTaskMan = subTaskMan;
    }

    public String getNewaddTaskMan() {
        return newaddTaskMan;
    }

    public void setNewaddTaskMan(String newaddTaskMan) {
        this.newaddTaskMan = newaddTaskMan;
    }
}
