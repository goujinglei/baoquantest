package com.andon.bean.dto;

import com.andon.bean.BaseEntity;

import java.io.Serializable;

public class RepairInput extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private int detailId;
    private String appearance;
    private String reportRepairTime;

    public int getDetailId() {
        return detailId;
    }

    public void setDetailId(int detailId) {
        this.detailId = detailId;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public String getReportRepairTime() {
        return reportRepairTime;
    }

    public void setReportRepairTime(String reportRepairTime) {
        this.reportRepairTime = reportRepairTime;
    }
}
