package com.andon.bean.dto;

import java.io.Serializable;

public class SpotDetailMould implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private int mouldId;
    private String vehicleType;
    private String figureNumber;
    private String assetCoding;
    private int state;
    private int confirmState;
    private int beginIndex;
    private int pageSize;

    public int getMouldId() {
        return mouldId;
    }

    public void setMouldId(int mouldId) {
        this.mouldId = mouldId;
    }

    public int getBeginIndex() {
        return beginIndex;
    }

    public void setBeginIndex(int beginIndex) {
        this.beginIndex = beginIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getFigureNumber() {
        return figureNumber;
    }

    public void setFigureNumber(String figureNumber) {
        this.figureNumber = figureNumber;
    }

    public String getAssetCoding() {
        return assetCoding;
    }

    public void setAssetCoding(String assetCoding) {
        this.assetCoding = assetCoding;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getConfirmState() {
        return confirmState;
    }

    public void setConfirmState(int confirmState) {
        this.confirmState = confirmState;
    }
}
