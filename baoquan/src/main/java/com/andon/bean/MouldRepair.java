package com.andon.bean;

public class MouldRepair extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	
	private int reId;
	
	private String mouldUser;  //模具担当
	
	private String shiftLeader;  //生产班长
	
	private String preservationDepartment;  //保全系长
	
	private String preservationSectionChief; //保全科长
	
	private String maintenanceDependencyDep;  //维修依赖部门
	
	private String maintenanceDependencyUse;  //维修依赖者

	private int warehouseTimeStart;  //先行品在库时间   开始
	
	private String warehouseTimeEnd;  //先行品在库时间    结束
	
	private int failurePeriod;  //故障时期 
	
	private String phenomenalDescription;  //故障内容
	
	private String reason;  //原因
	
	private String emergencyDisposal;  //紧急处置
	
	private String permanentGame;  //永久对策
	
	private String faultLocationUrl;  //故障位置照片
	
	private String productUrl;  //制品照片
	
	private String formingMachineUrl;  //成型机报警照片
	
	//传值使用
	private int mouldId;
	
	private String applicant;

	private String reportRepairTime;
	
	private String orderTime;
	
    private String beginTime;
    
    private String endTime;
    
    private String confirmationTime;
    
    private String preDepChiefConTime;
    
    private String preSecChiefConTime;
    
    private String figureNumber;
    
    private String model;
    
    private int state;
    
    private boolean flag;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMouldId() {
		return mouldId;
	}

	public void setMouldId(int mouldId) {
		this.mouldId = mouldId;
	}

	public String getMouldUser() {
		return mouldUser;
	}

	public void setMouldUser(String mouldUser) {
		this.mouldUser = mouldUser;
	}

	public String getShiftLeader() {
		return shiftLeader;
	}

	public void setShiftLeader(String shiftLeader) {
		this.shiftLeader = shiftLeader;
	}

	public String getPreservationDepartment() {
		return preservationDepartment;
	}

	public void setPreservationDepartment(String preservationDepartment) {
		this.preservationDepartment = preservationDepartment;
	}

	public String getMaintenanceDependencyDep() {
		return maintenanceDependencyDep;
	}

	public void setMaintenanceDependencyDep(String maintenanceDependencyDep) {
		this.maintenanceDependencyDep = maintenanceDependencyDep;
	}

	public String getMaintenanceDependencyUse() {
		return maintenanceDependencyUse;
	}

	public void setMaintenanceDependencyUse(String maintenanceDependencyUse) {
		this.maintenanceDependencyUse = maintenanceDependencyUse;
	}

	public int getWarehouseTimeStart() {
		return warehouseTimeStart;
	}

	public void setWarehouseTimeStart(int warehouseTimeStart) {
		this.warehouseTimeStart = warehouseTimeStart;
	}

	public String getWarehouseTimeEnd() {
		return warehouseTimeEnd;
	}

	public void setWarehouseTimeEnd(String warehouseTimeEnd) {
		this.warehouseTimeEnd = warehouseTimeEnd;
	}

	public int getFailurePeriod() {
		return failurePeriod;
	}

	public void setFailurePeriod(int failurePeriod) {
		this.failurePeriod = failurePeriod;
	}

	public String getPhenomenalDescription() {
		return phenomenalDescription;
	}

	public void setPhenomenalDescription(String phenomenalDescription) {
		this.phenomenalDescription = phenomenalDescription;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getEmergencyDisposal() {
		return emergencyDisposal;
	}

	public void setEmergencyDisposal(String emergencyDisposal) {
		this.emergencyDisposal = emergencyDisposal;
	}

	public String getPermanentGame() {
		return permanentGame;
	}

	public void setPermanentGame(String permanentGame) {
		this.permanentGame = permanentGame;
	}

	public String getFaultLocationUrl() {
		return faultLocationUrl;
	}

	public void setFaultLocationUrl(String faultLocationUrl) {
		this.faultLocationUrl = faultLocationUrl;
	}

	public String getProductUrl() {
		return productUrl;
	}

	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}

	public String getFormingMachineUrl() {
		return formingMachineUrl;
	}

	public void setFormingMachineUrl(String formingMachineUrl) {
		this.formingMachineUrl = formingMachineUrl;
	}

	public int getReId() {
		return reId;
	}

	public void setReId(int reId) {
		this.reId = reId;
	}

	public String getApplicant() {
		return applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getReportRepairTime() {
		return reportRepairTime;
	}

	public void setReportRepairTime(String reportRepairTime) {
		this.reportRepairTime = reportRepairTime;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getFigureNumber() {
		return figureNumber;
	}

	public void setFigureNumber(String figureNumber) {
		this.figureNumber = figureNumber;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getPreservationSectionChief() {
		return preservationSectionChief;
	}

	public void setPreservationSectionChief(String preservationSectionChief) {
		this.preservationSectionChief = preservationSectionChief;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

	public String getConfirmationTime() {
		return confirmationTime;
	}

	public void setConfirmationTime(String confirmationTime) {
		this.confirmationTime = confirmationTime;
	}

	public String getPreDepChiefConTime() {
		return preDepChiefConTime;
	}

	public void setPreDepChiefConTime(String preDepChiefConTime) {
		this.preDepChiefConTime = preDepChiefConTime;
	}

	public String getPreSecChiefConTime() {
		return preSecChiefConTime;
	}

	public void setPreSecChiefConTime(String preSecChiefConTime) {
		this.preSecChiefConTime = preSecChiefConTime;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
 
	
    
}
