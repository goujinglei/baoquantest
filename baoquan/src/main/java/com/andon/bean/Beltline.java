package com.andon.bean;

import java.io.Serializable;

public class Beltline  extends BaseEntity  implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private String beltlineName;
    private String beltlineDescription;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBeltlineName() {
        return beltlineName;
    }

    public void setBeltlineName(String beltlineName) {
        this.beltlineName = beltlineName;
    }

    public String getBeltlineDescription() {
        return beltlineDescription;
    }

    public void setBeltlineDescription(String beltlineDescription) {
        this.beltlineDescription = beltlineDescription;
    }
}
