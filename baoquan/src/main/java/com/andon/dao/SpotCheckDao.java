package com.andon.dao;

import java.util.List;

import com.andon.bean.SpotCheck;
import org.apache.ibatis.annotations.Param;

public interface SpotCheckDao {
    public void insert(List<SpotCheck> spotCheck);
    public List<SpotCheck> selectSpotCheck(SpotCheck spotCheck);
    public void deleteCheckList(List<Integer> ids);
    public void update(List<SpotCheck> spotCheck);
}
