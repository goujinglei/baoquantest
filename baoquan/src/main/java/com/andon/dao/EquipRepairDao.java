package com.andon.dao;

import com.andon.bean.EquipRepair;
import com.andon.bean.dto.EquipRepairOutput;
import com.andon.bean.dto.UpdateEquipRepair;
import org.apache.ibatis.annotations.Param;

public interface EquipRepairDao {
    public void insert(EquipRepair equipRepair);
    public void update(UpdateEquipRepair equipRepair);
    public EquipRepairOutput selectEquipRepair(@Param("id")int id);
}
