package com.andon.dao;

import org.apache.ibatis.annotations.Param;

import com.andon.bean.MouldRepair;

public interface MouldRepairDao {
		
	/**
	 * 新建模具报修
	 * 
	 * @param MouldRepair
	 */
	public void insertMouldRepair(MouldRepair mouldRepair);
	
	/**
	 * 根据模具报修表Id查询该条记录所有信息
	 * @param id
	 */
	public MouldRepair selectMouldRepairById(@Param("id") int id, @Param("isActive") int isActive);
	
	/**
	 * 更新模具报修表
	 * 
	 * @param MouldRepair
	 */
	public void updateMouldRepairById(MouldRepair mouldRepair);
	
	/**
	 * 班长确认
	 * 
	 * @param MouldRepair
	 */
	public void MouldRepairConfirm(MouldRepair mouldRepair);
}
