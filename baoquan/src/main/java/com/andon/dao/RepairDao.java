package com.andon.dao;

import com.andon.bean.Repair;
import com.andon.bean.dto.RepairOutput;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RepairDao {
    public int insert(Repair repair);
    public List<Repair> selectRepair(@Param("type")int type,@Param("state")int state,@Param("beginTime")String beginTime,@Param("endTime")String endTime,@Param("createUser")String createUser,@Param("workMan")String workMan,@Param("beginIndex")int beginIndex, @Param("pageSize")int pageSize);
    public int selectRepairCount(@Param("type")int type,@Param("state")int state,@Param("beginTime")String beginTime,@Param("endTime")String endTime,@Param("createUser")String createUser,@Param("workMan")String workMan);
    public void update(Repair repair);
}
