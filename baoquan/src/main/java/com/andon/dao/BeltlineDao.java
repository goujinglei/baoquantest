package com.andon.dao;

import com.andon.bean.Beltline;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface BeltlineDao {
    //添加生产线
    public void insert(Beltline beltline);
    public List<Beltline> selectLine(@Param("beginIndex")int beginIndex, @Param("pageSize")int pageSize);
    public int selectLineCount();
    //检索
    public List<Beltline> selectLineByFirm(@Param("beltlineName")String beltlineName,@Param("beltlineDescription")String beltlineDescription,@Param("beginIndex")int beginIndex, @Param("pageSize")int pageSize);
    public int selectLineByFirmCount(@Param("beltlineName")String beltlineName,@Param("beltlineDescription")String beltlineDescription);
    //更新
    public void updateLine(@Param("id")int id,@Param("beltlineName")String beltlineName,@Param("beltlineDescription")String beltlineDescription,@Param("updateUser")String updateUser,@Param("updateTime") Date updateTime);
    //删除
    public void deleteLine(@Param("id")int id);
    public Beltline selectLineByid(@Param("id")int id);
}
