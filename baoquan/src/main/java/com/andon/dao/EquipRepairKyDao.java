package com.andon.dao;

import com.andon.bean.EquipRepairKy;
import com.andon.bean.dto.WorkNameOutput;
import org.apache.ibatis.annotations.Param;

public interface EquipRepairKyDao {
    public void insert(EquipRepairKy equipRepairKy);
    WorkNameOutput selectWorkName(@Param("id")int id);
}
