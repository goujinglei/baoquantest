package com.andon.dao;

import com.andon.bean.Parts;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PartsDao {
    public void insert(List<Parts> parts);
    public List<Parts> selectParts(@Param("reId")int reId);
    public void delete(@Param("reId")int reId);
}
