package com.andon.dao;

import com.andon.bean.SpotTask;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SpotTaskDao {
    public int insert(List<SpotTask> spotTasks);
    public List<SpotTask> selectTask(@Param("spotType")int spotType,@Param("state")int state,@Param("spotInterval")String spotInterval,@Param("beginTime")String beginTime,@Param("endTime")String endTime,@Param("beginIndex")int beginIndex,@Param("pageSize")int pageSize);
    public int selectTaskCount(@Param("spotType")int spotType,@Param("state")int state,@Param("spotInterval")String spotInterval,@Param("beginTime")String beginTime,@Param("endTime")String endTime);
    public void deleteTask(@Param("id")int id);
    //检验相同设备相同周期的任务有没有在同月重复的记录
    public int selectTaskRepeat(SpotTask spotTask);

}
