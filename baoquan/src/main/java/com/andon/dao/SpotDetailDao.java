package com.andon.dao;

import com.andon.bean.SpotDetail;
import java.util.List;
import com.andon.bean.dto.SpotDetailEquip;
import com.andon.bean.dto.SpotDetailMould;
import org.apache.ibatis.annotations.Param;

public interface SpotDetailDao {
    public List<SpotDetailMould> selectSpotDetailMould(SpotDetailMould mould);
    public int selectSpotDetailMouldCount(SpotDetailMould mould);
    public List<SpotDetailEquip> selectSpotDetailEquip(SpotDetailEquip equip);
    public int selectSpotDetailEquipCount(SpotDetailEquip equip);
    public void deleteTaskDetail(@Param("id") int id);
    public List<SpotDetail> selectDeleteDetail(@Param("id") int id);

    public void updateState(@Param("id") int id);
    public void updateConfirmState(@Param("id") int id);
    //批量插入点检详情表
    public void insert(List<SpotDetail> spotDetail);
}
