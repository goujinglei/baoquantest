package com.andon.commons;

public class Constant {
	//是否有效中的有效
	public static final int ACTIVE_VALID = 1;
	//是否有效中的无效
	public static final int ACTIVE_DELETE = 0;
	//设备类型
	public static final int TYPE_EQUIP = 1;
	public static final int TYPE_MOULD = 2;

	//状态
	public static final int NEW_REPAIR = 1;
	public static final int REPAIR_DOING = 2;
	public static final int REPAIR_FINISH = 3;
	public static final int CONFIRM_FINISH = 4;
	public static final int CONFIRM_XIZHANG = 5;
	public static final int CONFIRM_KEZHANG = 6;

	public static final int STATE_IS_NULL = 9;

	//接单时间
	public static final String ORDER_TIME = "orderTime";
	//维修开始时间
	public static final String BEGIN_TIME ="beginTime";
	//维修结束时间
	public static final String END_TIME ="endTime";
	
	//工厂
	public static final int MANUFACTURER_ID_1 = 1;
	public static final int MANUFACTURER_ID_2 = 2;
	public static final int MANUFACTURER_ID_3 = 3;
	public static final String MANUFACTURER_NAME_1 = "天津";
	public static final String MANUFACTURER_NAME_2 = "长春";
	public static final String MANUFACTURER_NAME_3 = "佛山";


	//设备点检填表状态
	public static final int SPOT_ID_1 = 1;
	public static final int SPOT_ID_2 = 2;
	public static final int SPOT_ID_3 = 3;
	public static final int SPOT_ID_4 = 4;
	public static final String SPOT_NAME_1 = "计划";
	public static final String SPOT_NAME_2 = "完成";
	public static final String SPOT_NAME_3 = "不良要修理(不急)";
	public static final String SPOT_NAME_4 = "不良要修理(至急)";
	
	//点检详情表的状态
	public static final int SPOT_STATE_INCOMPLETE = 0;
	public static final int SPOT_STATE_COMPLETE = 1;

}
