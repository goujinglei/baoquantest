package com.andon.service;

import com.andon.bean.EquipRepair;
import com.andon.bean.dto.EquipRepairOutput;
import com.andon.bean.dto.UpdateEquipRepair;

public interface EquipRepairService {
    void add(EquipRepair equipRepair);
    void update(UpdateEquipRepair equipRepair);
    EquipRepairOutput getByid(int id);
}
