package com.andon.service;

import com.andon.bean.SpotTask;
import com.andon.bean.dto.TaskInput;

import java.util.List;


public interface SpotTaskService {

    void add(TaskInput taskInputs,String user, String[] ids) throws Exception;
    List<SpotTask> selectTask(int spotType, int state, String spotInterval, String beginTime, String endTime,int beginIndex,int pageSize);
    int selectTaskCount(int spotType,int state,String spotInterval,String beginTime,String endTime);
    void deleteTask(int id);
}
