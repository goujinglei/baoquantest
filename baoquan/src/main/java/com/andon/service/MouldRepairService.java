package com.andon.service;

import com.andon.bean.MouldRepair;

public interface MouldRepairService {

	/**
	 * 新建模具报修
	 * 
	 * @param MouldRepair
	 */
	public void insertMouldRepair(MouldRepair mouldRepair) throws Exception;
	
	/**
	 * 根据模具报修表Id查询该条记录所有信息
	 * @param id
	 */
	public MouldRepair selectMouldRepairById(int id) throws Exception;
	
	/**
	 * 更新模具报修表
	 * 
	 * @param MouldRepair
	 */
	public void updateMouldRepairById(MouldRepair mouldRepair) throws Exception;

	
	/**
	 * 班长确认
	 * @param id 
	 * 
	 * @param userName
	 * @param userType 
	 * @throws Exception 
	 */
	public void mouldRepairByMonitorConfirm(int id, String userName, int userType) throws Exception;
	
}
