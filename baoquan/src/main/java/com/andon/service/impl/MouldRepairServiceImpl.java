package com.andon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.andon.bean.MouldRepair;
import com.andon.bean.Repair;
import com.andon.commons.Constant;
import com.andon.dao.MouldRepairDao;
import com.andon.dao.RepairDao;
import com.andon.service.MouldRepairService;
import com.andon.utils.DateUtils;

@Service
@Transactional
public class MouldRepairServiceImpl implements MouldRepairService {
    @Autowired
    private MouldRepairDao mouldRepairDao;
    @Autowired
    private RepairDao repairDao;
    
	/**
	 * 新建模具报修
	 * 
	 * @param MouldRepair
	 */
	@Override
	public void insertMouldRepair(MouldRepair mouldRepair) throws Exception{
		//插入维修主表
		Repair repair = new Repair();
        repair.setDetailId(mouldRepair.getMouldId());
        repair.setType(Constant.TYPE_MOULD);
        repair.setState(Constant.NEW_REPAIR);
        repair.setApplicant(mouldRepair.getApplicant());
		repair.setReportRepairTime(DateUtils.getCurrentDateMinute());
		repair.setCreateUser(mouldRepair.getCreateUser());
		repair.setCreateTime(DateUtils.getNowDate());
		repair.setUpdateUser(mouldRepair.getUpdateUser());
		repair.setUpdateTime(DateUtils.getNowDate());
		repair.setIsActive(Constant.ACTIVE_VALID);
		repairDao.insert(repair);
		
		//插入模具维修表
		MouldRepair sql = mouldRepair;
		sql.setReId(repair.getId());
		sql.setCreateTime(DateUtils.getNowDate());
		sql.setUpdateTime(DateUtils.getNowDate());
		sql.setIsActive(Constant.ACTIVE_VALID);
		mouldRepairDao.insertMouldRepair(sql);
		
	}

	/**
	 * 根据模具报修表Id查询该条记录所有信息
	 * @param id
	 */
	@Override
	public MouldRepair selectMouldRepairById(int id) throws Exception{
		MouldRepair mouldRepair = mouldRepairDao.selectMouldRepairById(id, Constant.ACTIVE_VALID);
		return mouldRepair;
	}

	/**
	 * 更新模具报修表
	 * 
	 * @param MouldRepair
	 */
	@Override
	public void updateMouldRepairById(MouldRepair mouldRepair) throws Exception{
		//更新维修主表
		Repair repair = new Repair();
		repair.setId(mouldRepair.getReId());
//		repair.setBeginTime(mouldRepair.getBeginTime());
		if(mouldRepair.getState() != Constant.STATE_IS_NULL){
			
			repair.setEndTime(DateUtils.getCurrentDateMinute());
			repair.setState(Constant.REPAIR_FINISH);		
		}else{
			repair.setState(Constant.STATE_IS_NULL);		
			
		}
		
		repair.setUpdateTime(DateUtils.getNowDate());
		repair.setUpdateUser(mouldRepair.getUpdateUser());
		repair.setIsActive(Constant.ACTIVE_VALID);
		repairDao.update(repair);
		
		//更新模具维修表
		MouldRepair sql = mouldRepair;
		sql.setUpdateTime(DateUtils.getNowDate());
		sql.setIsActive(Constant.ACTIVE_VALID);
		mouldRepairDao.updateMouldRepairById(mouldRepair);
	}

	/**
	 * 班长确认
	 * @param id 
	 * 
	 * @param userName
	 */
	@Override
	public void mouldRepairByMonitorConfirm(int id, String userName, int userType) throws Exception{
		
		//更新维修主表状态
		Repair repair = new Repair();
		repair.setId(id);
		
		int start = 9;
		String time = DateUtils.getCurrentDateMinute();
		if(userType == 2){
			start = Constant.CONFIRM_FINISH;
			repair.setConfirmationTime(time);
		}else if(userType == 5){
			start = Constant.CONFIRM_XIZHANG;
			repair.setPreDepChiefConTime(time);
		}else if(userType == 6){
			start = Constant.CONFIRM_KEZHANG;
			repair.setPreSecChiefConTime(time);
		}
		
		repair.setState(start);
		repair.setUpdateTime(DateUtils.getNowDate());
		repair.setUpdateUser(userName);
		repair.setIsActive(Constant.ACTIVE_VALID);
		repairDao.update(repair);
		
		//更新设备维修表
		MouldRepair mouldRepair = new MouldRepair();
		mouldRepair.setReId(id);
		
		if(userType == 2){
			mouldRepair.setShiftLeader(userName);
		}else if(userType == 5){
			mouldRepair.setPreservationDepartment(userName);
		}else if(userType == 6){
			mouldRepair.setPreservationSectionChief(userName);
		}
		
		mouldRepair.setUpdateUser(userName);
		mouldRepair.setUpdateTime(DateUtils.getNowDate());
		mouldRepair.setIsActive(Constant.ACTIVE_VALID);
		mouldRepairDao.MouldRepairConfirm(mouldRepair);
	}


}
