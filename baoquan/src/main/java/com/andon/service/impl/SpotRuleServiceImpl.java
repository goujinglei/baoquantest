package com.andon.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tuckey.web.filters.urlrewrite.utils.StringUtils;

import com.andon.bean.Combox;
import com.andon.bean.Equip;
import com.andon.bean.SpotRule;
import com.andon.bean.dto.EquipSeeOutput;
import com.andon.commons.Constant;
import com.andon.dao.EquipDao;
import com.andon.dao.SpotRuleDao;
import com.andon.service.SpotRuleService;
import com.andon.utils.DateUtils;
import com.andon.utils.UUIDString;


@Service
@Transactional
public class SpotRuleServiceImpl implements SpotRuleService {
    @Autowired
    private SpotRuleDao spotRuleDao;
    @Autowired
    private EquipDao equipDao;

	@Override
	public void add(List<SpotRule> spotRule, String username) throws Exception{
		String groupKey = UUIDString.getPartsKeyByUUId();
		
		for(SpotRule s: spotRule){
			s.setGroupKey(groupKey);
			s.setCreateTime(DateUtils.getNowDate());
			s.setCreateUser(username);
			s.setUpdateTime(DateUtils.getNowDate());
			s.setUpdateUser(username);
			s.setIsActive(Constant.ACTIVE_VALID);
		}
		
		spotRuleDao.insert(spotRule);
		
	}

	@Override
	public List<SpotRule> getSpotRule(SpotRule spotRule) {
		return spotRuleDao.selectSpotRule(spotRule);
	}

    //规则列表
	@Override
	public List<SpotRule> selectRuleList(SpotRule spotRule) throws Exception{
		spotRule.setIsActive(Constant.ACTIVE_VALID);
		return spotRuleDao.selectRuleList(spotRule);
	}


	//删除规则
	@Override
	public void delRule(SpotRule spotRule) throws Exception{
		spotRule.setUpdateTime(DateUtils.getNowDate());
		spotRule.setIsActive(Constant.ACTIVE_DELETE);
		spotRuleDao.delRule(spotRule);
		
	}

	//一条规则详情
	@Override
	public List<SpotRule> ruleDetail(SpotRule spotRule) throws Exception {
		spotRule.setIsActive(Constant.ACTIVE_VALID);
		
		return spotRuleDao.ruleDetail(spotRule);
	}

	//编辑规则
	@Override
	public void edit(List<SpotRule> spotRule, String userName, String groupKey) throws Exception {

		//先删除
		spotRuleDao.updateRulToDel(groupKey, Constant.ACTIVE_VALID);
		//后插入
		for(SpotRule s: spotRule){
			s.setGroupKey(groupKey);
			s.setCreateTime(DateUtils.getNowDate());
			s.setCreateUser(userName);
			s.setUpdateTime(DateUtils.getNowDate());
			s.setUpdateUser(userName);
			s.setIsActive(Constant.ACTIVE_VALID);
		}
		
		spotRuleDao.insert(spotRule);
	}

	//查询设备型号
	@Override
	public int selectEquipModel(String equipModel) throws Exception{

		return equipDao.selectEquipModel(equipModel, Constant.ACTIVE_VALID);
	}

	//效验重复
	@Override
	public int Validation(List<SpotRule> spotRule) throws Exception{
		spotRule.get(0).setIsActive(Constant.ACTIVE_VALID);
		return spotRuleDao.validation(spotRule.get(0));
	}

	//根据设备型号查询设备列表
	@Override
	public List<Combox> selectEquipModelListByEquipModel(String equipModel, String[] ids) throws Exception {
		List<Combox> combox = new ArrayList<Combox>();
		List<EquipSeeOutput> equipList = equipDao.selectEquipModelListByEquipModel(equipModel, Constant.ACTIVE_VALID);		
		List<EquipSeeOutput> equipList2 = new ArrayList<EquipSeeOutput>();
		
		if((equipList != null && equipList.size() > 0) && (ids == null || ids.length == 0)){
			
			equipList2 = equipList;
			
		}else if((equipList != null && equipList.size() > 0) && (ids != null && ids.length > 0)){
			
			List<String> eIds = new ArrayList<String>();
			for(int i =0; i< ids.length; i++){
				eIds.add(ids[i]);
			}
			
			equipList2 = equipList.stream().filter((EquipSeeOutput a) -> !eIds.contains(String.valueOf(a.getId()))).distinct().collect(Collectors.toList());
		}
		

        if(equipList2 != null && equipList2.size() > 0){
    		for(EquipSeeOutput e : equipList2){
    			Combox c = new Combox();
    			c.setId(e.getId());
    			//组装数据
                String selectText = formatPartString(e.getBeltlineName()) + formatPartString(e.getEquipNum()) + 
                		formatPartString(e.getEquipName()) + formatPartString(e.getEquipModel());
                c.setName(selectText);
                combox.add(c);
    		}
        }
	
		return combox;
	}

	private String formatPartString(String data){
		if(data.length() < 15){
			int l = data.length();
			for(int i = 0; i< (15 - l); i++){
				data += "&nbsp;&nbsp;";
			}
		}
		return data;
	}

	//根据设备型号模糊查询设备列表
	@Override
	public List<EquipSeeOutput> selectEquipModelList(Equip equip) throws Exception {
		List<EquipSeeOutput> equipSeeOutput = equipDao.selectEquipModelList(equip.getEquipModel(), Constant.ACTIVE_VALID);
		
		SpotRule sql = new SpotRule();
		sql.setType(1);
		sql.setIsActive(Constant.ACTIVE_VALID);
		List<SpotRule> spotRule = spotRuleDao.selectSpotRuleByAll(sql);
		
		if(equipSeeOutput != null && equipSeeOutput.size() > 0){
			if(spotRule != null && spotRule.size() > 0){
				for(int i = 0; i < equipSeeOutput.size(); i++){
					
					for(int j = 0; j < spotRule.size(); j++){
						
						if(equipSeeOutput.get(i).getEquipModel().equals(spotRule.get(j).getClassification())){
							
							String cycleString = equipSeeOutput.get(i).getCycleString() == null? "": equipSeeOutput.get(i).getCycleString();
							
							if(StringUtils.isBlank(cycleString)){
								cycleString += cycleToString(spotRule.get(j).getCycle());
							}else{
								cycleString += "," + cycleToString(spotRule.get(j).getCycle());
							}
							
							equipSeeOutput.get(i).setCycleString(cycleString);
						}
					}
					
				}
			}
		}
		return equipSeeOutput;
	}


	//根据设备型号模糊查询设备列表数量
	@Override
	public int selectEquipModelCount(Equip equip) throws Exception {

		return equipDao.selectEquipModelCount(equip.getEquipModel(), Constant.ACTIVE_VALID);
	}
	
	private String cycleToString(String cycle){
		String cycleString = "";
		switch(cycle){
		case "1": cycleString = "月检";
			break;
		case "3": cycleString = "季度检";
			break;
		case "6": cycleString = "半年检";
			break;
		case "12": cycleString = "年检";
			break;
		default: cycleString = "";
		
		}
		return cycleString;
	}
}
