package com.andon.service.impl;

import com.andon.bean.Repair;
import com.andon.bean.dto.RepairOutput;
import com.andon.dao.RepairDao;
import com.andon.service.RepairService;
import com.andon.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RepairServiceImpl implements RepairService {

    @Autowired
    private RepairDao repairDao;

    @Override
    public int add(Repair repair) {
        repairDao.insert(repair);
        return repair.getId();
    }

    @Override
    public List<Repair> getRepair(int type, int state, String beginTime, String endTime, String createUser,String workMan,int beginIndex, int pageSize) {
        return repairDao.selectRepair(type,state,beginTime,endTime,createUser,workMan,beginIndex,pageSize);
    }

    @Override
    public int getRepairCount(int type, int state, String beginTime, String endTime, String createUser,String workMan) {
        return repairDao.selectRepairCount(type,state,beginTime,endTime,createUser,workMan);
    }

    @Override
    public void update(Repair repair) {
        repairDao.update(repair);
    }
}
