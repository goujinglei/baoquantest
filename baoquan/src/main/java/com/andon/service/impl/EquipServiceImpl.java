package com.andon.service.impl;

import com.andon.bean.Equip;
import com.andon.bean.dto.EquipSeeOutput;
import com.andon.dao.EquipDao;
import com.andon.service.EquipService;
import com.andon.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EquipServiceImpl implements EquipService {
    @Autowired
    private EquipDao equipDao;

    @Override
    public void add(Equip equip) {
        equipDao.insert(equip);
    }

    @Override
    public List<Equip> getEquip(int beginIndex, int pageSize) {
        return equipDao.selectEquip(beginIndex,pageSize);
    }

    @Override
    public int getEquipCount() {
        return equipDao.selectEquipCount();
    }

    @Override
    public List<Equip> getEquipByFirm(int factory,String equipName, String equipDescription, String useBeginTime, String equipModel, String standard, int beginIndex, int pageSize) {
        return equipDao.selectEquipByFirm(factory,equipName,equipDescription,useBeginTime,equipModel,standard,beginIndex,pageSize);
    }

    @Override
    public int getEquipByFirmCount(int factory,String equipName, String equipDescription, String useBeginTime, String equipModel, String standard) {
        return equipDao.selectEquipByFirmCount(factory,equipName,equipDescription,useBeginTime,equipModel,standard);
    }

    @Override
    public Equip getByid(int id) {
        return equipDao.selectEquipByid(id);
    }

    @Override
    public void update(Equip equip) {
        equipDao.update(equip);
    }
    @Override
    public void updateEqLine(int id, int lineId) {
        equipDao.updateEqLine(id,lineId);
    }

    @Override
    public void deleteEq(int id) {
        equipDao.deleteEq(id);
    }

    @Override
    public List<Equip> getEqBylineID(int lineId) {
        return equipDao.selectEqBylineID(lineId);
    }

    @Override
    public void deleteEqLine(List<Equip> equip) {
        equipDao.deleteEqLine(equip);
    }

    @Override
    public EquipSeeOutput getEquipSee(int id) {
        return equipDao.selectEquipSee(id);
    }
}
