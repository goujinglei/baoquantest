package com.andon.service.impl;

import com.andon.bean.EquipRepair;
import com.andon.bean.dto.EquipRepairOutput;
import com.andon.bean.dto.UpdateEquipRepair;
import com.andon.dao.EquipRepairDao;
import com.andon.service.EquipRepairService;
import com.andon.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EquipRepairServiceImpl implements EquipRepairService {
    @Autowired
    private EquipRepairDao equipRepairDao;
    @Override
    public void add(EquipRepair equipRepair) {
        equipRepairDao.insert(equipRepair);
    }

    @Override
    public void update(UpdateEquipRepair equipRepair) {
        equipRepairDao.update(equipRepair);
    }

    @Override
    public EquipRepairOutput getByid(int id) {
        return equipRepairDao.selectEquipRepair(id);
    }
}
