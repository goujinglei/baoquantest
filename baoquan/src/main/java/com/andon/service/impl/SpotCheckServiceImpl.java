package com.andon.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.andon.bean.SpotCheck;
import com.andon.bean.SpotRule;
import com.andon.commons.Constant;
import com.andon.dao.SpotCheckDao;
import com.andon.dao.SpotRuleDao;
import com.andon.service.SpotCheckService;
import com.andon.utils.DateUtils;

import net.sf.json.JSONArray;

@Service
@Transactional
public class SpotCheckServiceImpl implements SpotCheckService {
    @Autowired
    private SpotCheckDao spotCheckDao;

    @Autowired
	private SpotRuleDao spotRuleDao;

	@Override
	public void add(List<SpotCheck> spotCheck, String userName) {
		spotCheckDao.insert(spotCheck);
	}

	@Override
	public List<SpotCheck> getSpotCheck(SpotCheck spotCheck,String userName) {
		List<SpotCheck> spotChecks = spotCheckDao.selectSpotCheck(spotCheck);
		if(spotChecks.size()==0){
			SpotRule spotRule = new SpotRule();
			spotRule.setType(spotCheck.getType());
			spotRule.setClassification(spotCheck.getPrincipalNumbe());
			spotRule.setCycle(spotCheck.getSpotInterval());
			List<SpotRule> spotRules = spotRuleDao.selectSpotRule(spotRule);
			if(spotRules.size()>0) {
				List<SpotCheck> checkList = new ArrayList<SpotCheck>();
				for (SpotRule spot : spotRules) {
					SpotCheck check = new SpotCheck();
					check.setSpotDetailId(spotCheck.getSpotDetailId());
					check.setType(spotCheck.getType());
					check.setSpotInterval(spotCheck.getSpotInterval());
					check.setPrincipalNumbe(spotCheck.getPrincipalNumbe());
					check.setPrincipalName(spotCheck.getPrincipalName());
					check.setDepartment(spotCheck.getDepartment());
					check.setSpotPosition(spot.getSpotPosition());
					check.setCheckProject(spot.getCheckProject());
					check.setCheckMethod(spot.getCheckMethod());
					check.setRemarks(spot.getRemarks());
					check.setCreateUser(userName);
					check.setUpdateUser(userName);
					check.setCreateTime(DateUtils.getNowDate());
					check.setUpdateTime(DateUtils.getNowDate());
					check.setIsActive(Constant.ACTIVE_VALID);
					check.setPrictureUrl(spot.getPrictureUrl());
					checkList.add(check);
				}
				spotCheckDao.insert(checkList);
			}
			spotChecks = spotCheckDao.selectSpotCheck(spotCheck);
		}
		return spotChecks;
	}

	@Override
	public void updateSpotCheck(List<SpotCheck> listJson, String name) {
		JSONArray jsonArray=JSONArray.fromObject(listJson);
		Collection collection  = JSONArray.toCollection(jsonArray, SpotCheck.class);
		Iterator it = collection.iterator();
		while (it.hasNext()) {
            List<SpotCheck> selectedQueryJudgement = new ArrayList<SpotCheck>();
			SpotCheck q = (SpotCheck) it.next();
			q.setUpdateUser(name);
			q.setUpdateTime(DateUtils.getNowDate());
			selectedQueryJudgement.add(q);
            spotCheckDao.update(selectedQueryJudgement);
		}
	}
}
