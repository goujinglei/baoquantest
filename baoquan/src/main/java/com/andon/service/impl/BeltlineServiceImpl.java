package com.andon.service.impl;

import com.andon.bean.Beltline;
import com.andon.dao.BeltlineDao;
import com.andon.service.BeltlineService;
import com.andon.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class BeltlineServiceImpl implements BeltlineService {
    @Autowired
    private BeltlineDao beltlineDao;

    @Override
    public void add(Beltline beltline) {
        beltlineDao.insert(beltline);
    }

    @Override
    public List<Beltline> getLine(int beginIndex, int pageSize) {
        return beltlineDao.selectLine(beginIndex,pageSize);
    }

    @Override
    public int getLineCount() {
        return beltlineDao.selectLineCount();
    }

    @Override
    public List<Beltline> getLineByFirm(String beltlineName, String beltlineDescription, int beginIndex, int pageSize) {
        return beltlineDao.selectLineByFirm(beltlineName,beltlineDescription,beginIndex,pageSize);
    }

    @Override
    public int getLineByFirmCount(String beltlineName, String beltlineDescription) {
        return beltlineDao.selectLineByFirmCount(beltlineName,beltlineDescription);
    }

    @Override
    public void updateLine(int id, String beltlineName, String beltlineDescription, String updateUser, Date updateTime) {
        beltlineDao.updateLine(id,beltlineName,beltlineDescription,updateUser,updateTime);
    }

    @Override
    public void deleteLine(int id) {
        beltlineDao.deleteLine(id);
    }

    @Override
    public Beltline getLineByid(int id) {
        return beltlineDao.selectLineByid(id);
    }
}
