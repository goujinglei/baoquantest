package com.andon.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.andon.bean.Mould;
import com.andon.commons.Constant;
import com.andon.dao.MouldDao;
import com.andon.service.MouldService;
import com.andon.utils.DateUtils;

@Service
@Transactional
public class MouldServiceImpl implements MouldService {
    @Autowired
    private MouldDao mouldDao;


	/**
	 * 根据条件检索模具表数量
	 * @param Mould
	 * @return int
	 */
	@Override
	public int getMouldCount(Mould mould) throws Exception{
		mould.setIsActive(Constant.ACTIVE_VALID);
		return mouldDao.getMouldCount(mould);
	}

	/**
	 * 根据条件检索模具表列表
	 * @param Mould 
	 * @return List
	 */	
	@Override
	public List<Mould> getMouldList(Mould mould) throws Exception{
		mould.setIsActive(Constant.ACTIVE_VALID);
		return mouldDao.getMouldList(mould);
	}

	/**
	 * 新建模具
	 * 
	 * @param Mould
	 */
	@Override
	public void insertMould(Mould mould) {
		mould.setCreateTime(DateUtils.getNowDate());
		mould.setUpdateTime(DateUtils.getNowDate());
		mould.setIsActive(Constant.ACTIVE_VALID);
		mouldDao.insertMould(mould);
		
	}

	/**
	 * 效验名是否重复
	 * 
	 * @param Mould
	 */
	@Override
	public Integer checkForRecurrence(Mould mould) {
		mould.setIsActive(Constant.ACTIVE_VALID);
		return mouldDao.checkForRecurrence(mould);
	}

	/**
	 * 根据模具表Id查询该条记录所有信息
	 * @param id
	 */
	@Override
	public Mould selectMouldById(int id) {

		return mouldDao.selectMouldById(id, Constant.ACTIVE_VALID);
	}

	
	/**
	 * 更新模具
	 * 
	 * @param Mould
	 */
	@Override
	public void updateMouldById(Mould mould) {
		mould.setUpdateTime(DateUtils.getNowDate());
		mould.setIsActive(Constant.ACTIVE_VALID);
		mouldDao.updateMouldById(mould);
		
	}

    //根据模具的车种和图号搜索唯一的 车种记录
	@Override
	public Mould selectMouldByVehicleTypeAndFigureNumber(Mould mould) {
		mould.setIsActive(Constant.ACTIVE_VALID);
		Mould mouldToView = mouldDao.selectMouldByVehicleTypeAndFigureNumber(mould);
		if(mouldToView != null){
			switch(mouldToView.getFactory()){
			case 1: mouldToView.setFactoryName(Constant.MANUFACTURER_NAME_1);
				break;
			case 2: mouldToView.setFactoryName(Constant.MANUFACTURER_NAME_2);
				break;
			case 3: mouldToView.setFactoryName(Constant.MANUFACTURER_NAME_2);
				break;
			default:
				mouldToView.setFactoryName("");
			}
		}

		return mouldToView;
	}

}
