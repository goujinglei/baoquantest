package com.andon.service.impl;

import com.andon.bean.EquipRepairKy;
import com.andon.bean.dto.WorkNameOutput;
import com.andon.dao.EquipRepairKyDao;
import com.andon.service.EquipRepairKyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EquipRepairKyServiceImpl implements EquipRepairKyService {
    @Autowired
    private EquipRepairKyDao equipRepairKyDao;
    @Override
    public void add(EquipRepairKy equipRepairKy) {
        equipRepairKyDao.insert(equipRepairKy);
    }

    @Override
    public WorkNameOutput selectWorkName(int id) {
        return equipRepairKyDao.selectWorkName(id);
    }
}
