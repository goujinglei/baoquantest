package com.andon.service.impl;

import com.andon.bean.dto.SpotDetailEquip;
import com.andon.bean.dto.SpotDetailMould;
import com.andon.dao.SpotDetailDao;
import com.andon.service.SpotDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SpotDetailServiceImpl implements SpotDetailService {

    @Autowired
    private SpotDetailDao spotDetailDao;
    @Override
    public List<SpotDetailMould> getSpotDetailMould(SpotDetailMould mould) {
        return spotDetailDao.selectSpotDetailMould(mould);
    }

    @Override
    public int getSpotDetailMouldCount(SpotDetailMould mould) {
        return spotDetailDao.selectSpotDetailMouldCount(mould);
    }

    @Override
    public List<SpotDetailEquip> getSpotDetailEquip(SpotDetailEquip equip) {
        return spotDetailDao.selectSpotDetailEquip(equip);
    }

    @Override
    public int getSpotDetailEquipCount(SpotDetailEquip equip) {
        return spotDetailDao.selectSpotDetailEquipCount(equip);
    }

    @Override
    public void updateState(int id) {
        spotDetailDao.updateState(id);
    }

    @Override
    public void updateConfirmState(int id) {
        spotDetailDao.updateConfirmState(id);
    }
}
