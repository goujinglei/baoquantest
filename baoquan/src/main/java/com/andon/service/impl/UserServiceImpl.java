package com.andon.service.impl;

import com.andon.bean.User;
import com.andon.dao.UserDao;
import com.andon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Override
    public User getUser(String username, String password) {
        return userDao.selectUser(username,password);
    }

    @Override
    public List<User> getUserBaoquan() {
        return userDao.selectUserBaoquan();
    }

    @Override
    public User getDepartment(String username) {
        return userDao.selectDepartment(username);
    }
}
