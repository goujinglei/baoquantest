package com.andon.service.impl;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.andon.bean.SpotCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.andon.bean.SpotDetail;
import com.andon.bean.SpotTask;
import com.andon.bean.dto.TaskInput;
import com.andon.commons.Constant;
import com.andon.dao.SpotCheckDao;
import com.andon.dao.SpotDetailDao;

import com.andon.dao.SpotTaskDao;
import com.andon.exception.TaskDuplicationException;
import com.andon.service.SpotTaskService;
import com.andon.utils.DateUtils;

@Service
@Transactional
public class SpotTaskServiceImpl implements SpotTaskService {
    @Autowired
    private SpotTaskDao spotTaskDao;
    @Autowired
    private SpotDetailDao spotDetailDao;
    @Autowired
    private SpotCheckDao spotCheckDao;

    @Override
    public void add(TaskInput taskInputs,String user, String[] ids) throws Exception{

        List<SpotTask> spotTasks = new ArrayList<SpotTask>();
        int times = taskInputs.getTimes();
        for(int i=0;i<times;i++){
            SpotTask spotTask = new SpotTask();
            spotTask.setSpotName(taskInputs.getSpotName());
            //得到每个顺延任务的开始时间
            String beginTime = getTimeToBegin(taskInputs.getSpotType(), taskInputs.getSpotInterval(),taskInputs.getBeginTime(),i, times);
            //验证相同设备相同周期的点检开始时间一样的未完成的记录
            spotTaskVerifyByBeginTime(taskInputs, ids, beginTime);

            spotTask.setBeginTime(beginTime);
            spotTask.setEndTime(getTimeToEnd(taskInputs.getSpotType(), taskInputs.getSpotInterval(),taskInputs.getBeginTime(),i+1, times));
            spotTask.setSpotInterval(taskInputs.getSpotInterval());
            spotTask.setSpotType(taskInputs.getSpotType());
            spotTask.setCreateUser(user);
            spotTask.setUpdateUser(user);
            spotTask.setCreateTime(DateUtils.getNowDate());
            spotTask.setUpdateTime(DateUtils.getNowDate());
            spotTask.setIsActive(Constant.ACTIVE_VALID);
            spotTask.setState(0);
            spotTasks.add(spotTask);
        }
        
        //插入点检详情表
        if(spotTasks != null || spotTasks.size() > 0){
        	       	       	
            spotTaskDao.insert(spotTasks);
            List<SpotDetail> spotDetailList = new ArrayList<SpotDetail>();
            
            for(SpotTask s: spotTasks){
            	for(int i=0; i<ids.length; i++){
            		SpotDetail spotDetail = new SpotDetail();
            		spotDetail.setTaskId(s.getId());
            		spotDetail.setType(s.getSpotType());
            		spotDetail.setDetailId(Integer.parseInt(ids[i]));
            		spotDetail.setState(Constant.SPOT_STATE_INCOMPLETE);
            		spotDetail.setConfirmState(0);
            		spotDetail.setCreateUser(user);
            		spotDetail.setUpdateUser(user);
            		spotDetail.setCreateTime(DateUtils.getNowDate());
            		spotDetail.setUpdateTime(DateUtils.getNowDate());
            		spotDetail.setIsActive(Constant.ACTIVE_VALID);
            		
            		spotDetailList.add(spotDetail);
            	}
            }
            
            if(spotDetailList != null || spotDetailList.size() > 0){
            	spotDetailDao.insert(spotDetailList);
            }
        }
    }

	private void spotTaskVerifyByBeginTime(TaskInput taskInputs, String[] ids, String beginTime) {
		for(int j=0; j<ids.length; j++){
		    //查询是否已经存在相同设备相同周期开始时间为同1天的记录
		    SpotTask spotTaskVerify = new SpotTask();
		    spotTaskVerify.setBeginTime(beginTime);
		    spotTaskVerify.setSpotType(taskInputs.getSpotType());
		    spotTaskVerify.setSpotInterval(taskInputs.getSpotInterval());
		    spotTaskVerify.setState(Constant.SPOT_STATE_INCOMPLETE);
		    spotTaskVerify.setIsActive(Constant.ACTIVE_VALID);
		    spotTaskVerify.setDetailId(Integer.parseInt(ids[0]));
		    int count = spotTaskDao.selectTaskRepeat(spotTaskVerify);
		    if(count > 0){
		    	throw new TaskDuplicationException(ids[0] + "-" + taskInputs.getSpotType());
		    }
		}
	}

    @Override
    public List<SpotTask> selectTask(int spotType, int state, String spotInterval, String beginTime, String endTime,int beginIndex,int pageSize) {
        return spotTaskDao.selectTask(spotType,state,spotInterval,beginTime,endTime,beginIndex,pageSize);
    }

    @Override
    public int selectTaskCount(int spotType, int state, String spotInterval, String beginTime, String endTime) {
        return spotTaskDao.selectTaskCount(spotType,state,spotInterval,beginTime,endTime);
    }

    @Override
    public void deleteTask(int id) {
        List<Integer> integers = new ArrayList<>();
        List<SpotCheck> spotChecks = new ArrayList<SpotCheck>();
        List<SpotDetail> spotDetails =  spotDetailDao.selectDeleteDetail(id);
        if(spotDetails!=null&&spotDetails.size()>0){
            for(SpotDetail spotDetail:spotDetails){
                integers.add(spotDetail.getId());
            }
            spotCheckDao.deleteCheckList(integers);
        }
        spotDetailDao.deleteTaskDetail(id);
        //删除主表
        spotTaskDao.deleteTask(id);
    }

    //计算开始时间
    private String getTimeToBegin(int type, String interval, String date,int index, int times){
        Date date1 = DateUtils.stringToDate(date,DateUtils.DATE_FORMAT);
        if(type == 1){
            if(interval.equals("1")){
                return DateUtils.dateToString(DateUtils.addMonth(date1,1*index));
            }else if(interval.equals("3")){
                return DateUtils.dateToString(DateUtils.addMonth(date1,3*index));
            } else if(interval.equals("6")){
                return DateUtils.dateToString(DateUtils.addMonth(date1,6*index));
            }else {
                return DateUtils.dateToString(DateUtils.addYear(date1,1*index));
            }
        }else {
        	return DateUtils.dateToString(DateUtils.addMonth(date1, (12/times)*index));
        }

    }
    
    //计算结束时间
    private String getTimeToEnd(int type, String interval, String date,int index, int times){
        Date date1 = DateUtils.stringToDate(date,DateUtils.DATE_FORMAT);
        if(type == 1){
            if(interval.equals("1")){
                return DateUtils.dateToString(DateUtils.addDay(DateUtils.addMonth(date1,1*index), -1));
            }else if(interval.equals("3")){
                return DateUtils.dateToString(DateUtils.addDay(DateUtils.addMonth(date1,3*index), -1));
            } else if(interval.equals("6")){
                return DateUtils.dateToString(DateUtils.addDay(DateUtils.addMonth(date1,6*index), -1));
            }else {
                return DateUtils.dateToString(DateUtils.addDay(DateUtils.addYear(date1,1*index), -1));
            }
        }else {
        	return DateUtils.dateToString(DateUtils.addDay(DateUtils.addMonth(date1, (12/times)*index), -1));
        }

    }
}
