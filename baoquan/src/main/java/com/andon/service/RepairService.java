package com.andon.service;

import com.andon.bean.Repair;
import com.andon.bean.dto.RepairOutput;

import java.util.List;

public interface RepairService {
    //添加
    int add(Repair repair);
    List<Repair> getRepair(int type,int state,String beginTime,String endTime,String createUser,String workMan,int beginIndex,int pageSize);
    int getRepairCount(int type,int state,String beginTime,String endTime,String createUser,String workMan);
    void update(Repair repair);
}
