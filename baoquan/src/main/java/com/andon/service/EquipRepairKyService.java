package com.andon.service;

import com.andon.bean.EquipRepairKy;
import com.andon.bean.dto.WorkNameOutput;

public interface EquipRepairKyService {
    void add(EquipRepairKy equipRepairKy);
    WorkNameOutput selectWorkName(int id);
}
