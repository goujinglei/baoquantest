package com.andon.service;

import com.andon.bean.Beltline;

import java.util.Date;
import java.util.List;

public interface BeltlineService {
    void add(Beltline beltline);
    List<Beltline> getLine(int beginIndex, int pageSize);
    int getLineCount();
    List<Beltline> getLineByFirm(String beltlineName,String beltlineDescription,int beginIndex, int pageSize);
    int getLineByFirmCount(String beltlineName,String beltlineDescription);
    void updateLine(int id, String beltlineName, String beltlineDescription, String updateUser, Date updateTime);
    void deleteLine(int id);
    Beltline getLineByid(int id);
}
