package com.andon.service;

import com.andon.bean.User;

import java.util.List;

public interface UserService {
    User getUser(String username,String password);
    List<User> getUserBaoquan();
    User getDepartment(String username);
}
