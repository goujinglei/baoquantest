package com.andon.controller;


import com.andon.bean.EquipRepair;
import com.andon.bean.Repair;
import com.andon.bean.dto.RepairInput;
import com.andon.commons.Constant;
import com.andon.commons.ExceptionCode;
import com.andon.service.EquipRepairService;
import com.andon.service.RepairService;
import com.andon.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/repair")
public class RepairController extends BaseController{

    @Autowired
    private RepairService repairService;

    @Autowired
    private EquipRepairService equipRepairService;
    //添加修理
    @RequestMapping(value = "/addRepair.do", method = RequestMethod.POST)
    public @ResponseBody Object addRepair(HttpServletRequest request, HttpSession httpSession, @RequestBody RepairInput dataJson) {
        try {
            String user = null;
            user = httpSession.getAttribute("username").toString();
            Repair repair = new Repair();
            repair.setDetailId(dataJson.getDetailId());
            repair.setApplicant(user);
            repair.setType(Constant.TYPE_EQUIP);
            repair.setState(Constant.NEW_REPAIR);
            repair.setReportRepairTime(dataJson.getReportRepairTime());
            repair.setCreateUser(user);
            repair.setUpdateUser(user);
            repair.setCreateTime(DateUtils.getNowDate());
            repair.setUpdateTime(DateUtils.getNowDate());
            repair.setIsActive(Constant.ACTIVE_VALID);
            //返回主表id
            int id = repairService.add(repair);
            EquipRepair equipRepair = new EquipRepair();
            equipRepair.setReId(id);
            equipRepair.setOperator(user);
            equipRepair.setAppearance(dataJson.getAppearance());
            equipRepair.setCreateUser(user);
            equipRepair.setUpdateUser(user);
            equipRepair.setCreateTime(DateUtils.getNowDate());
            equipRepair.setUpdateTime(DateUtils.getNowDate());
            equipRepair.setIsActive(Constant.ACTIVE_VALID);
            equipRepairService.add(equipRepair);
            return resultHandler(null);
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }

    //维修一览
    @RequestMapping(value = "/getRepair.do", method = RequestMethod.POST)
    public @ResponseBody Object getRepair(HttpServletRequest request, int type,int state,String beginTime,String endTime,String createUser,String workMan,int beginIndex,int pageSize) {
        try {
            return resultHandler(repairService.getRepair(type,state,beginTime,endTime,createUser,workMan,beginIndex,pageSize));
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }
    //分页用
    @RequestMapping(value = "/getRepairCount.do", method = RequestMethod.POST)
    public @ResponseBody Object getRepairCount(HttpServletRequest request, int type,int state,String beginTime,String endTime,String createUser,String workMan) {
        try {
            return resultHandler(repairService.getRepairCount(type,state,beginTime,endTime,createUser,workMan));
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }

    //更新
    @RequestMapping(value = "/update.do", method = RequestMethod.POST)
    public @ResponseBody Object update(HttpServletRequest request,HttpSession httpSession, Repair repair) {

        try {
            // 设置接单时间
            if(repair.getOrderTime() != null && repair.getOrderTime().length() != 0){
                if(repair.getOrderTime().equals(Constant.ORDER_TIME)){
                    repair.setOrderTime(DateUtils.getCurrentDateMinute());
                }
            }
            //设置维修开始时间
            if(repair.getBeginTime() != null && repair.getBeginTime().length() != 0){
                if(repair.getBeginTime().equals(Constant.BEGIN_TIME)){
                    repair.setBeginTime(DateUtils.getCurrentDateMinute());
                }
            }
            //更新状态班长系长科长确认时间
            if(repair.getState()==4){
                repair.setConfirmationTime(DateUtils.getCurrentDateMinute());
            }else if(repair.getState()==5){
                repair.setPreDepChiefConTime(DateUtils.getCurrentDateMinute());
            }else if(repair.getState()==6){
                repair.setPreSecChiefConTime(DateUtils.getCurrentDateMinute());
            }
            String user = httpSession.getAttribute("username").toString();
            repair.setUpdateUser(user);
            repair.setUpdateTime(DateUtils.getNowDate());
            repair.setIsActive(Constant.ACTIVE_VALID);
            repairService.update(repair);
            return resultHandler(null);
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }
}
