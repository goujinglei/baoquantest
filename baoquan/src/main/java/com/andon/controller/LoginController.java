package com.andon.controller;

import com.andon.bean.User;
import com.andon.bean.UserBaseInfo;
import com.andon.commons.ExceptionCode;
import com.andon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/login")
public class LoginController extends BaseController {

    @Autowired
    private UserService userService;
    //存储用户信息到session
    @ResponseBody
    @RequestMapping(value = "/loginIn.do", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public Object loginIn(HttpServletRequest request, HttpSession httpSession) {
        try {
            String loginuser = null;
            loginuser = request.getParameter("username");
            String pwd = request.getParameter("password");
            //TODO
            //调用api 存储session
            User user = userService.getUser(loginuser,pwd);
            if(user==null){
                return resultHandler(exceptionHandle(ExceptionCode.ERROR_USERNAME_OR_PWD));
            }
            UserBaseInfo userBaseInfo = new UserBaseInfo();
            userBaseInfo.setUsername(loginuser);
            userBaseInfo.setPassword(pwd);
            userBaseInfo.setUserType(user.getUserType());
            // 将用户保存到session内
            httpSession.setAttribute("username", loginuser);
            httpSession.setAttribute("password", pwd);
            httpSession.setAttribute("userType",user.getUserType());

//            ServletContext application = httpSession.getServletContext();
//            @SuppressWarnings("unchecked")
//            Map<String, Object> loginMap = (Map<String, Object>) application.getAttribute("loginMap");
//            if (loginMap == null) {
//                loginMap = new HashMap<String, Object>();
//            }
//            for (String key : loginMap.keySet()) {
//                if (loginuser.equals(key)) {
//                    if (httpSession.getId().equals(loginMap.get(key))) {
//                        return resultHandler(exceptionHandle(ExceptionCode.LOGIN_SAME_ERROR));
//                     //在同一地点重复登录
//                    }
////                    else {
////                        //return resultHandler(exceptionHandle(ExceptionCode.LOGIN_EARE_ERROR));
////                     //异地已登录，请先退出登录
////                    }
//                }
//            }
//            loginMap.put(httpSession.getId(),user);
//            application.setAttribute("loginMap", loginMap);
//            // 将用户保存在session当中
//            httpSession.setAttribute("user", user);
            // session 销毁时间
            //httpSession.setMaxInactiveInterval(5);
            return resultHandler(null);
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }

    }

    //获取用户session信息
    @ResponseBody
    @RequestMapping(value = "/getUserSession.do", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public Object getUserSession(HttpServletRequest request, HttpSession httpSession) {

        try {
//            ServletContext application = httpSession.getServletContext();
//            Map<String, Object> loginMap = (Map<String, Object>) application.getAttribute("loginMap");
//            for (String key : loginMap.keySet()) {
//                if (httpSession.getAttribute("username").toString().equals(key)) {
//                    if (!httpSession.getId().equals(loginMap.get(key))) {
//                        return resultHandler(exceptionHandle(ExceptionCode.LOGIN_EARE_ERROR));
//                    }
//                }
//            }
            UserBaseInfo userBaseInfo = null;
            userBaseInfo = new UserBaseInfo();
            if (httpSession != null) {
                userBaseInfo.setUsername(httpSession.getAttribute("username").toString());
                userBaseInfo.setPassword(httpSession.getAttribute("password").toString());
                userBaseInfo.setUserType(Integer.parseInt(httpSession.getAttribute("userType").toString()));
            }
            return resultHandler(userBaseInfo);
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }

//    //退登陆
//    @ResponseBody
//    @RequestMapping(value = "/logout.do", method = RequestMethod.POST)
//    public Object logout(HttpServletRequest request, HttpSession httpSession) {
//
//        String loginuser = request.getParameter("username");
//        String pwd = request.getParameter("password");
//        //TODO
//        //调用api 存储session
//        User user = userService.getUser(loginuser,pwd);
//
//        HttpSession session = request.getSession();
//        String user_id = request.getParameter(String.valueOf(user.getId()));
//        if (session != null) {
//            try {
//                session.removeAttribute("user");
//                ServletContext application = session.getServletContext();
//                application.removeAttribute("loginMap");
//            } catch (Exception e) {
//                return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
//            }
//        }
//        return resultHandler(null);
//    }


}


