package com.andon.controller;

import com.andon.bean.SpotDetail;
import com.andon.bean.dto.SpotDetailEquip;
import com.andon.bean.dto.SpotDetailMould;
import com.andon.commons.ExceptionCode;
import com.andon.service.SpotDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/spotDetail")
public class SpotDetailController extends BaseController{
    @Autowired
    private SpotDetailService spotDetailService;

    //获取模具
    @RequestMapping(value = "/getDetailMould.do", method = RequestMethod.POST)
    public @ResponseBody Object getDetailMould(HttpServletRequest request, SpotDetailMould spotDetailMould) {

        try {
            return resultHandler(spotDetailService.getSpotDetailMould(spotDetailMould));
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }

    //获取模具
    @RequestMapping(value = "/getDetailMouldCount.do", method = RequestMethod.POST)
    public @ResponseBody Object getDetailMouldCount(HttpServletRequest request, SpotDetailMould spotDetailMould) {

        try {
            return resultHandler(spotDetailService.getSpotDetailMouldCount(spotDetailMould));
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }

    //获取设备
    @RequestMapping(value = "/getDetailEquip.do", method = RequestMethod.POST)
    public @ResponseBody Object getDetailEquip(HttpServletRequest request, SpotDetailEquip spotDetailEquip) {

        try {
            return resultHandler(spotDetailService.getSpotDetailEquip(spotDetailEquip));
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }

    //获取设备
    @RequestMapping(value = "/getDetailEquipCount.do", method = RequestMethod.POST)
    public @ResponseBody Object getDetailEquipCount(HttpServletRequest request, SpotDetailEquip spotDetailEquip) {

        try {
            return resultHandler(spotDetailService.getSpotDetailEquipCount(spotDetailEquip));
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }

    //更新状态
    @RequestMapping(value = "/updateState.do", method = RequestMethod.POST)
    public @ResponseBody Object updateState(HttpServletRequest request, SpotDetail spotDetail) {

        try {
            spotDetailService.updateState(spotDetail.getId());
            return resultHandler(null);
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }

    //更新确认状态
    @RequestMapping(value = "/updateConfirmState.do", method = RequestMethod.POST)
    public @ResponseBody Object updateConfirmState(HttpServletRequest request, SpotDetail spotDetail) {

        try {
            spotDetailService.updateConfirmState(spotDetail.getId());
            return resultHandler(null);
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }
}
