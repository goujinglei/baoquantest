package com.andon.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.andon.bean.Combox;
import com.andon.bean.Mould;
import com.andon.bean.MouldRepair;
import com.andon.bean.TestMould;
import com.andon.commons.Constant;
import com.andon.commons.ExceptionCode;
import com.andon.service.MouldRepairService;
import com.andon.service.MouldService;
import com.andon.service.TestMouldService;
import com.andon.utils.UUIDString;

@Controller
@RequestMapping("/mould")
public class MouldController extends BaseController {

    @Autowired
    private MouldService mouldService;
    
    @Autowired
    private MouldRepairService mouldRepairService;
    
    @Autowired
    private TestMouldService testMouldService;


    //模具列表信息
    @RequestMapping(value = "/getMouldList.do", method = RequestMethod.POST)
    public @ResponseBody Object getMouldList(HttpServletRequest request, Mould mould) {
    	
		try {

			List<Mould> mouldList = mouldService.getMouldList(mould);
			return resultHandler(mouldList);

		} catch (Exception e) {
			e.printStackTrace();
			return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
		}
    	
    }

    //模具列表数量
    @RequestMapping(value = "/getMouldCount.do", method = RequestMethod.POST)
    public @ResponseBody Object getMouldCount(HttpServletRequest request, Mould mould) {
		try {

			int pageCount;

			pageCount = mouldService.getMouldCount(mould);

			return resultHandler(pageCount);
			
		} catch (Exception e) {
			e.printStackTrace();
			return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
		}
    }

    //插入
    @RequestMapping(value = "/insertMould.do", method = RequestMethod.POST)
    @ResponseBody
    public Object insertMould(HttpServletRequest request, Mould mould, HttpSession session)throws ParseException {
		try {
			//效验重复
			int count = mouldService.checkForRecurrence(mould);
			if(count > 0){
				return resultHandler(exceptionHandle(ExceptionCode.MOULD_REPEAT));
			}
			
//			File targetFile=null;
//			String url="";//返回存储路径
//			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request; 
//			MultipartFile pictureUrl =  multipartRequest.getFile("picture3D");
//			
//			
//			 String fileName=pictureUrl.getOriginalFilename();//获取文件名加后缀
//		        if(fileName!=null&&fileName!=""){
//		            String returnUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() +"/metronic/upload/";//存储路径
//		            String path = request.getSession().getServletContext().getRealPath("metronic/upload"); //文件存储位置
//		            String fileF = fileName.substring(fileName.lastIndexOf("."), fileName.length());//文件后缀
//		            fileName=new Date().getTime()+"_"+new Random().nextInt(1000)+fileF;//新的文件名
//
//		            //先判断文件是否存在
//		            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//		            String fileAdd = sdf.format(new Date());
//		            //获取文件夹路径
//		            File file1 =new File(path+"/"+fileAdd);
//		            //如果文件夹不存在则创建
//		            if(!file1 .exists()  && !file1 .isDirectory()){
//		                file1 .mkdir();
//		            }
//		            //将图片存入文件夹
//		            targetFile = new File(file1, fileName);
//	                //将上传的文件写到服务器上指定的文件。
//		            pictureUrl.transferTo(targetFile);
//	                url = returnUrl + fileAdd+"/"+fileName;
//                    
//		        }
		        
			mould.setMouldKey(UUIDString.getPartsKeyByUUId());
//		    mould.setPictureUrl(url);
			String userName = session.getAttribute("username").toString();
			mould.setCreateUser(userName);
			mould.setUpdateUser(userName);
			mouldService.insertMould(mould);
			return resultHandler(null);
//		} catch (MaxUploadSizeExceededException e) {
//			return exceptionHandle(ExceptionCode.FILE_UPLOAD_FAILED);
//		} catch (IOException e) {
//			return exceptionHandle(ExceptionCode.FORMATFILE_NOT_IMAGE);

		} catch (Exception e) {
			e.printStackTrace();
			return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
		}
    }


    //根据id查询模具详情
    @RequestMapping(value = "/selectMouldById.do", method = RequestMethod.POST)
    public @ResponseBody Object selectMouldById(HttpServletRequest request, int id) {
		try {

			Mould mould = mouldService.selectMouldById(id);
			return resultHandler(mould);
			
		} catch (Exception e) {

			return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
		}
    	
    }

    //修改
    @RequestMapping(value = "/updateMouldById.do", method = RequestMethod.POST)
    public @ResponseBody Object updateMouldById(HttpServletRequest request, Mould mould, HttpSession session) {
		try {
//			File targetFile=null;
//			String url="";//返回存储路径
				
//			try {
//				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request; 
//				MultipartFile pictureUrl =  multipartRequest.getFile("picture3D");
//							
//				String fileName=pictureUrl.getOriginalFilename();//获取文件名加后缀
//				if(fileName!=null&&fileName!=""){
//				    String returnUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() +"/metronic/upload/";//存储路径
//				    String path = request.getSession().getServletContext().getRealPath("metronic/upload"); //文件存储位置
//				    String fileF = fileName.substring(fileName.lastIndexOf("."), fileName.length());//文件后缀
//				    fileName=new Date().getTime()+"_"+new Random().nextInt(1000)+fileF;//新的文件名
//
//				    //先判断文件是否存在
//				    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//				    String fileAdd = sdf.format(new Date());
//				    //获取文件夹路径
//				    File file1 =new File(path+"/"+fileAdd);
//				    //如果文件夹不存在则创建
//				    if(!file1 .exists()  && !file1 .isDirectory()){
//				        file1 .mkdir();
//				    }
//				    //将图片存入文件夹
//				    targetFile = new File(file1, fileName);
//				    //将上传的文件写到服务器上指定的文件。
//				    pictureUrl.transferTo(targetFile);
//				    url = returnUrl + fileAdd+"/"+fileName;
//				    
//				}
//			} catch (ClassCastException e) {
//				
//			}
//		    mould.setPictureUrl(url);   
			String userName = session.getAttribute("username").toString();
			mould.setUpdateUser(userName);
			mouldService.updateMouldById(mould);
			return resultHandler(null);
//		} catch (MaxUploadSizeExceededException e) {
//			return exceptionHandle(ExceptionCode.FILE_UPLOAD_FAILED);
//		} catch (IOException e) {
//			return exceptionHandle(ExceptionCode.FORMATFILE_NOT_IMAGE);

		} catch (Exception e) {
			e.printStackTrace();
			return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
		}
    }
    
    //报修
    @RequestMapping(value = "/repairMould.do", method = RequestMethod.POST)
    public @ResponseBody Object repairMould(MouldRepair mouldRepair, HttpServletRequest request,  HttpSession session) {
    	
		try {
			String userName = session.getAttribute("username").toString();
			mouldRepair.setCreateUser(userName);
			mouldRepair.setUpdateUser(userName);
			mouldRepairService.insertMouldRepair(mouldRepair);
			
			return resultHandler(null);
		} catch (Exception e) {
			e.printStackTrace();
			return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
		}
    }
    
    //保全第一次填写KY后填写维修信息
    @RequestMapping(value = "/selectRepairMouldByRepairId.do", method = RequestMethod.POST)
    public @ResponseBody Object selectRepairMouldByRepairId(int id, HttpServletRequest request,  HttpSession session) {
    	
		try {
             
			MouldRepair mouldRepair = mouldRepairService.selectMouldRepairById(id);
			
			//查询试模记录中是否已经存在成功的记录
			int count = testMouldService.selectTestMouldToSucceedById(id);
			if(count > 0){
				mouldRepair.setFlag(true);
			}else{
				mouldRepair.setFlag(false);
			}
			
			return resultHandler(mouldRepair);
		} catch (Exception e) {
			e.printStackTrace();
			return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
		}
    }
    
    //保全第一次填写KY后填写维修信息
    @RequestMapping(value = "/selectRepairMouldByConfirm.do", method = RequestMethod.POST)
    public @ResponseBody Object selectRepairMouldByConfirm(int id, HttpServletRequest request,  HttpSession session) {
    	
		try {
             
			MouldRepair mouldRepair = mouldRepairService.selectMouldRepairById(id);
			
			//查询试模记录
			List<TestMould> testMouldList = testMouldService.getTestMouldList(id);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("MouldRepair", mouldRepair);
			map.put("TestMouldList", testMouldList);
			
			return resultHandler(map);
		} catch (Exception e) {
			e.printStackTrace();
			return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
		}
    }
    
    //保全填写或补全信息
    @RequestMapping(value = "/updateRepairMould.do", method = RequestMethod.POST)
    public @ResponseBody Object updateRepairMould(MouldRepair mouldRepair, HttpServletRequest request,  HttpSession session) {
    	
		try {
			String userName = session.getAttribute("username").toString();
			mouldRepair.setCreateUser(userName);
			mouldRepair.setUpdateUser(userName);
			mouldRepairService.updateMouldRepairById(mouldRepair);
			
			return resultHandler(null);
		} catch (Exception e) {
			e.printStackTrace();
			return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
		}
    }
    
    //班长确认
    @RequestMapping(value = "/mouldRepairByMonitorConfirm.do", method = RequestMethod.GET)
    public @ResponseBody Object mouldRepairByMonitorConfirm(int id, HttpServletRequest request,  HttpSession session) {
    	
		try {
			String userName = session.getAttribute("username").toString();
			int userType = Integer.parseInt(session.getAttribute("userType").toString());

			mouldRepairService.mouldRepairByMonitorConfirm(id, userName, userType);
			
			return resultHandler(null);
		} catch (Exception e) {
			e.printStackTrace();
			return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
		}
    }
    
	/**
	 * 查询所有下拉框
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/initCombox.do", method = RequestMethod.GET)
	public @ResponseBody Object initCombox(HttpServletRequest request) {
	    
	    //工厂combox
		List<Combox> manufacturerCombox = new ArrayList<Combox>();
		Combox c1 = new Combox();
		c1.setId(Constant.MANUFACTURER_ID_1);
		c1.setName(Constant.MANUFACTURER_NAME_1);
		manufacturerCombox.add(c1);
		Combox c2 = new Combox();
		c2.setId(Constant.MANUFACTURER_ID_2);
		c2.setName(Constant.MANUFACTURER_NAME_2);
		manufacturerCombox.add(c2);
		Combox c3 = new Combox();
		c3.setId(Constant.MANUFACTURER_ID_3);
		c3.setName(Constant.MANUFACTURER_NAME_3);
		manufacturerCombox.add(c3);


		return resultHandler(manufacturerCombox);
	}
}
