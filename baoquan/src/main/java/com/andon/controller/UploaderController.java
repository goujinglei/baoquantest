package com.andon.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.andon.commons.ExceptionCode;

@Controller
@RequestMapping ("/file")
public class UploaderController extends BaseController{


//	@RequestMapping("/batchFileUpload.do")
//    @ResponseBody
//    public Map<String,Object> batchFileUpload(@RequestParam(value = "file") MultipartFile file) {
//        //批量上传
//        String  str = Upload.batchFileUpload(file);
//        //输出文件地址
//        System.out.println("地址是:"+str);
//		return str;
//    }
	

//	//上传图片  img
//	@RequestMapping("/fileUpload")
//    public String fileUpload(@RequestParam(value = "file", required = false)
//       MultipartFile file,HttpServletRequest request,HttpServletResponse response){
//		response.setCharacterEncoding("utf-8");
//    	//String path = request.getServletContext().getRealPath("/images/book/"); 
//      String path="f:/easyuidemoimage/";
//    	String fileName = file.getOriginalFilename();  //prefix  suffix
//    	String  suffix=fileName.substring(fileName.lastIndexOf("."));
//      String newFileName=	UUID.randomUUID().toString()+suffix;
//      File targetFile = new File(path, newFileName);
//      if(!targetFile.exists()){  
//          targetFile.mkdirs(); 
//      }  
//      try {  
//          file.transferTo(targetFile);  
//          System.out.println("上传成功:"+path+newFileName);
//          response.getWriter().write("文件上传成功:"+path+newFileName);
//      } catch (Exception e) {  
//          e.printStackTrace();  
//      }  
//    	return null;
//    }
//	@RequestMapping("/toFileUpload")
//	public String toFileUpload(){
//		return "/book/fileUpload.jsp";
//	}
	
//    @RequestMapping("/ajaxFileUpload")
//	 public  @ResponseBody Object  ajaxFileUpload(HttpServletRequest request,HttpServletResponse response,
//			 @RequestParam(value = "file") MultipartFile file){
//   	 Map<String, Object> map = new HashMap<String, Object>();
//   	 try {
//   	      String path="f:/easyuidemoimage/";
//   	      String fileName = file.getOriginalFilename();  //prefix  suffix
//   	      String  suffix=fileName.substring(fileName.lastIndexOf("."));
//   	      String newFileName=	UUID.randomUUID().toString()+suffix;
//   	      File targetFile = new File(path, newFileName);
//   	      if(!targetFile.exists()){  
//   	          targetFile.mkdirs(); 
//   	      }  
//   	      file.transferTo(targetFile);  
//             System.out.println("上传成功:"+path+newFileName);
//             
//             map.put("rc", 1);
//             map.put("msg", "上传成功");
//             map.put("value", newFileName);
//             
//             return  resultHandler(map);
//          
//		} catch (Exception e) {
//			e.printStackTrace();
//			
//		}
//   	 return  resultHandler(map);
//    }
	
     @RequestMapping("/ajaxFileUpload")
	 public  @ResponseBody Object  ajaxFileUpload(HttpServletRequest request,HttpServletResponse response,
			 @RequestParam(value = "file") MultipartFile file){
    	 
    	 Map<String, Object> map = new HashMap<String, Object>();
         File targetFile=null;
         String url="";//返回存储路径
         String fileName=file.getOriginalFilename();//获取文件名加后缀
         
         if(fileName!=null&&fileName!=""){
             String returnUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() +"/metronic/upload/";//存储路径
             String path = request.getSession().getServletContext().getRealPath("metronic/upload"); //文件存储位置
             String fileF = fileName.substring(fileName.lastIndexOf("."), fileName.length());//文件后缀
             fileName=new Date().getTime()+"_"+new Random().nextInt(1000)+fileF;//新的文件名

             //先判断文件是否存在
             SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
             String fileAdd = sdf.format(new Date());
             //获取文件夹路径
             File file1 =new File(path+"/"+fileAdd);
             //如果文件夹不存在则创建
             if(!file1 .exists()  && !file1 .isDirectory()){
                 file1 .mkdir();
             }
             //将图片存入文件夹
             targetFile = new File(file1, fileName);
             try {
                 //将上传的文件写到服务器上指定的文件。
                 file.transferTo(targetFile);
                 url=returnUrl+fileAdd+"/"+fileName;

                 map.put("rc", 1);
                 map.put("msg", "上传成功");
                 map.put("value", url);
                 return  resultHandler(map);
             } catch (Exception e) {
                e.printStackTrace();
    			return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
             }
         }else{
             map.put("rc", 2);
             map.put("msg", "上传失败");
             map.put("value", null);
             return  resultHandler(map);
         }


     }

}
