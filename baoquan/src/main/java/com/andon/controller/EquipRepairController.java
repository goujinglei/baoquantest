package com.andon.controller;


import com.andon.bean.EquipRepair;
import com.andon.bean.MouldRepair;
import com.andon.bean.Parts;
import com.andon.bean.User;
import com.andon.bean.dto.EquipRepairOutput;
import com.andon.bean.dto.UpdateEquipRepair;
import com.andon.commons.ExceptionCode;
import com.andon.service.EquipRepairService;
import com.andon.service.MouldRepairService;
import com.andon.service.PartsService;
import com.andon.service.UserService;
import com.andon.utils.DateUtils;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@Controller
@RequestMapping("/equipRepair")
public class EquipRepairController extends BaseController{
    @Autowired
    private UserService userService;
    @Autowired
    private EquipRepairService equipRepairService;
    @Autowired
    private MouldRepairService mouldRepairService;
    @Autowired
    private PartsService partsService;
    //修改
    @RequestMapping(value = "/update.do", method = RequestMethod.POST)
    public @ResponseBody Object update(HttpServletRequest request, HttpSession session, @RequestBody UpdateEquipRepair updateEquipRepair) {

        //更新部品信息
        try {
            List<Parts> partsList = partsService.selectParts(updateEquipRepair.getId());
            if(partsList!=null&&partsList.size()>0){
                partsService.delete(updateEquipRepair.getId());
            }
            String insertList = updateEquipRepair.getPartsListStr();
            if(!"".equals(insertList) && insertList != null) {
                JSONArray jsonArray=JSONArray.fromObject(insertList);
                Collection collection  = JSONArray.toCollection(jsonArray, Parts.class);
                Iterator it = collection.iterator();
                List<Parts> partsList1 = new ArrayList<Parts>();
                while (it.hasNext()) {
                    Parts q = (Parts) it.next();
                    q.setReId(updateEquipRepair.getId());
                    q.setIsActive(1);
                    partsList1.add(q);
                }
                partsService.add(partsList1);
            }
            String userName = session.getAttribute("username").toString();
            updateEquipRepair.setUpdateUser(userName);
            updateEquipRepair.setUpdateTime(DateUtils.getNowDate());
            equipRepairService.update(updateEquipRepair);
            return resultHandler(null);
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }

    @RequestMapping(value = "/getInfo.do", method = RequestMethod.POST)
    public @ResponseBody Object getInfo(HttpServletRequest request, int id) {

        try {
            EquipRepairOutput equipRepairOutput = null;
            equipRepairOutput = equipRepairService.getByid(id);
            List<Parts> outparts = new ArrayList<>();
            List<Parts> partsList = partsService.selectParts(id);
            if(partsList != null && partsList.size()>0){
                for (Parts parts:partsList){
                    outparts.add(parts);
                }
                equipRepairOutput.setPartsList(outparts);
            }
            return resultHandler(equipRepairOutput);
        } catch (Exception e) {
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }
    }

    //修改
    @RequestMapping(value = "/updateMould.do", method = RequestMethod.POST)
    public @ResponseBody Object updateMould(HttpServletRequest request, MouldRepair MouldRepair) {
        try {
            mouldRepairService.updateMouldRepairById(MouldRepair);
            return resultHandler(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resultHandler(exceptionHandle(ExceptionCode.ERROR_CODE));
        }

    }
}
